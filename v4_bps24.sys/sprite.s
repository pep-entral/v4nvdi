        include "const.s"

        XREF sp_struct      
        
        TEXT
  
        XDEF sprite_draw
        XDEF sprite_undraw

sprite_draw:
      MOVEQ     #$F,D2
      MOVEQ     #$F,D3
      MOVEQ     #0,D6 
      MOVEQ     #0,D7 
      MOVE.B    7(A0),D6
      MOVE.B    9(A0),D7
      LSL.L     #3,D6 
      LSL.L     #3,D7 
      MOVEA.L   sp_struct,A1
      MOVEA.L   632(A1),A1
      LEA       48(A1,D6.L),A3
      MOVE.W    (A3)+,D6
      MOVE.B    (A3),D6 
      ADDQ.L    #2,A3 
      SWAP      D6
      MOVE.W    (A3)+,D6
      MOVE.B    (A3),D6 
      LEA       48(A1,D7.L),A3
      MOVE.W    (A3)+,D7
      MOVE.B    (A3),D7 
      ADDQ.L    #2,A3 
      SWAP      D7
      MOVE.W    (A3)+,D7
      MOVE.B    (A3),D7 
      MOVEA.L   sp_struct,A1
      BTST      #7,419(A1)
      BEQ.S     .l1 
      ROL.W     #8,D6 
      ROL.W     #8,D7 
      SWAP      D6
      SWAP      D7
      ROL.W     #8,D6 
      ROL.W     #8,D7 
.l1:  SUB.W     (A0)+,D0
      BPL.S     .l2 
      ADD.W     D0,D2 
      BMI       .done 
.l2:  MOVE.W    _DEV_TAB.W,D4
      SUBI.W    #$F,D4
      SUB.W     D0,D4 
      BGE.S     .l3 
      ADD.W     D4,D2 
      BMI       .done 
.l3:  SUB.W     (A0)+,D1
      ADDQ.L    #6,A0 
      BPL.S     .l4 
      ADD.W     D1,D3 
      BMI       .done 
      ADD.W     D1,D1 
      ADD.W     D1,D1 
      SUBA.W    D1,A0 
      MOVEQ     #0,D1 
.l4:  MOVE.W    (_DEV_TAB+2).W,D5
      SUBI.W    #$F,D5
      SUB.W     D1,D5 
      BGE.S     .l5 
      ADD.W     D5,D3 
      BMI       .done 
.l5:  MOVE.W    D3,(A2) 
      ADDQ.W    #1,(A2)+
      MULS      _bytes_lin.W,D1
      MOVEA.L   v_bas_ad.W,A1 
      ADDA.L    D1,A1 
      MOVEA.L   A1,A4 
      MOVE.W    D0,D4 
      CMP.W     #$F,D2
      BEQ.S     .l6 
      MOVEQ     #0,D4 
      TST.W     D0
      BMI.S     .l6 
      MOVEQ     #-$F,D4 
      ADD.W     _DEV_TAB.W,D4
.l6:  ADDA.W    D4,A4
      ADDA.W    D4,A4 
      ADDA.W    D4,A4 
      MOVE.L    A4,(A2)+
      MOVE.W    #$100,(A2)+ 
      MOVEA.W   _bytes_lin.W,A3
      LEA       -48(A3),A3
      MOVE.W    D3,D5 
.l7:  MOVE.L    (A4)+,(A2)+ 
      MOVE.L    (A4)+,(A2)+ 
      MOVE.L    (A4)+,(A2)+ 
      MOVE.L    (A4)+,(A2)+ 
      MOVE.L    (A4)+,(A2)+ 
      MOVE.L    (A4)+,(A2)+ 
      MOVE.L    (A4)+,(A2)+ 
      MOVE.L    (A4)+,(A2)+ 
      MOVE.L    (A4)+,(A2)+ 
      MOVE.L    (A4)+,(A2)+ 
      MOVE.L    (A4)+,(A2)+ 
      MOVE.L    (A4)+,(A2)+ 
      ADDA.W    A3,A4 
      DBF       D5,.l7
      MOVEA.W   _bytes_lin.W,A3
      SUBA.W    D2,A3 
      SUBA.W    D2,A3  
      SUBA.W    D2,A3 
      SUBQ.W    #3,A3 
      ADDA.W    D0,A1 
      ADDA.W    D0,A1 
      ADDA.W    D0,A1 
      CMP.W     #$F,D2
      BEQ.S     .l8 
      TST.W     D0
      BPL.S     .l8 
      SUBA.W    D0,A1 
      SUBA.W    D0,A1 
      SUBA.W    D0,A1 
      NEG.W     D0
      BRA.S     .l9 
      
.l8:  MOVEQ     #0,D0 
.l9:  MOVE.W    (A0)+,D4
      MOVE.W    (A0)+,D5
      LSL.W     D0,D4 
      LSL.W     D0,D5 
      MOVE.W    D2,D1 
.l10: ADD.W     D5,D5 
      BCC.S     .l11 
      MOVE.B    D7,(A1)+
      MOVE.B    D7,(A1)+
      MOVE.B    D7,(A1)+
      ADD.W     D4,D4 
      DBF       D1,.l10
      BRA.S     .l13 
.l11: ADD.W     D4,D4 
      BCC.S     .l12 
      MOVE.B    D6,(A1)+
      MOVE.B    D6,(A1)+
      MOVE.B    D6,(A1)+
      DBF       D1,.l10
      BRA.S     .l13 
.l12: ADDQ.L    #3,A1 
      DBF       D1,.l10
.l13: ADDA.W    A3,A1 
      DBF       D3,.l9
      
.done:RTS 

sprite_undraw:
      MOVE.W    (A2)+,D2
      SUBQ.W    #1,D2 
      BMI.S     .done 
      MOVEA.L   (A2)+,A1
      BCLR      #0,(A2) 
      BEQ.S     .done 
      MOVEA.W   _bytes_lin.W,A3
      LEA       -48(A3),A3
      ADDQ.L    #2,A2 
.l1:  MOVE.L    (A2)+,(A1)+ 
      MOVE.L    (A2)+,(A1)+ 
      MOVE.L    (A2)+,(A1)+ 
      MOVE.L    (A2)+,(A1)+ 
      MOVE.L    (A2)+,(A1)+ 
      MOVE.L    (A2)+,(A1)+ 
      MOVE.L    (A2)+,(A1)+ 
      MOVE.L    (A2)+,(A1)+ 
      MOVE.L    (A2)+,(A1)+ 
      MOVE.L    (A2)+,(A1)+ 
      MOVE.L    (A2)+,(A1)+ 
      MOVE.L    (A2)+,(A1)+ 
      ADDA.W    A3,A1 
      DBF       D2,.l1
      
.done:RTS 
    