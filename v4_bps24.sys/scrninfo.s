      include "const.s"

      TEXT

      XDEF    scrninfo
        
scrninfo:
      MOVEM.L   A0-A1/D0-D1,-(A7) 
      LEA       .scrntab_1,A1
      MOVE.W    (A1)+,(A0)+ 
      MOVE.W    (A1)+,(A0)+ 
      MOVE.W    (A1)+,(A0)+ 
      MOVE.L    (A1)+,(A0)+ 
      MOVE.W    _bytes_lin.W,(A0)+ 
      MOVE.L    v_bas_ad.W,(A0)+
      ADDQ.L    #6,A1 
      MOVE.W    (A1)+,(A0)+ 
      MOVE.W    (A1)+,(A0)+ 
      MOVE.W    (A1)+,(A0)+ 
      MOVE.W    (A1)+,(A0)+ 
      MOVE.W    (A1)+,(A0)+ 
      MOVE.W    (A1)+,(A0)+ 
      MOVE.W    (A1)+,(A0)+ 
      MOVE.W    (A1)+,(A0)+ 
      MOVE.W    #$FF,D0 
.l1:  MOVE.W    (A1)+,(A0)+ 
      DBF       D0,.l1
      MOVEM.L   (A7)+,A0-A1/D0-D1 
      RTS 

.scrntab_1:

      ; Basic screen format [0... 15]
      DC.W        2           ; Pixel format (packed pixels)
      DC.W        2           ; Clut flag (software CLUT; HiColor or TrueColor)
      DC.W        24          ; Number of planes (bits) per pixel
      DC.L        $01000000   ; Number of colours
      DC.W        0           ; Line width in bytes (set by code above)
      DC.L        0           ; Address to bitmap   (set by code above)
      DC.W        8,8,8       ; Number of bits for red/green/blue levels
      DC.W        0           ; Number of bits for alpha
      DC.W        0           ; Number of bits for genlock
      DC.W        0           ; Number of unused bits
      DC.W        1           ; Bit order
      DC.W        0           ; (unused?)
  
      ; Bits for red [16... 31]
      DC.W        16          ;
      DC.W        17          ;
      DC.W        18          ;
      DC.W        19          ;
      DC.W        20          ;
      DC.W        21          ;
      DC.W        22          ;
      DC.W        23          ;
      DC.W        $ffff       ;
      DC.W        $ffff       ;
      DC.W        $ffff       ;
      DC.W        $ffff       ;
      DC.W        $ffff       ;
      DC.W        $ffff       ;
      DC.W        $ffff       ;
      DC.W        $ffff       ;

      ; Bits for green [32... 47]
      DC.W        8           ;
      DC.W        9           ;
      DC.W        10          ;
      DC.W        11          ;
      DC.W        12          ;
      DC.W        13          ;
      DC.W        14          ;
      DC.W        15          ;
      DC.W        $ffff       ;
      DC.W        $ffff       ;
      DC.W        $ffff       ;
      DC.W        $ffff       ;
      DC.W        $ffff       ;
      DC.W        $ffff       ;
      DC.W        $ffff       ;
      DC.W        $ffff       ;

      ; Bits for blue [48... 63]
      DC.W        0           ;
      DC.W        1           ;
      DC.W        2           ;
      DC.W        3           ;
      DC.W        4           ;
      DC.W        5           ;
      DC.W        6           ;
      DC.W        7           ;
      DC.W        $ffff       ;
      DC.W        $ffff       ;
      DC.W        $ffff       ;
      DC.W        $ffff       ;
      DC.W        $ffff       ;
      DC.W        $ffff       ;
      DC.W        $ffff       ;
      DC.W        $ffff       ;

      ; Bits for alpha [64... 79]
      DC.W        $ffff       ;
      DC.W        $ffff       ;
      DC.W        $ffff       ;
      DC.W        $ffff       ;
      DC.W        $ffff       ;
      DC.W        $ffff       ;
      DC.W        $ffff       ;
      DC.W        $ffff       ;
      DC.W        $ffff       ;
      DC.W        $ffff       ;
      DC.W        $ffff       ;
      DC.W        $ffff       ;
      DC.W        $ffff       ;
      DC.W        $ffff       ;
      DC.W        $ffff       ;
      DC.W        $ffff       ;

      ; Bits for genlock [80... 95]
      DC.W        $ffff       ;
      DC.W        $ffff       ;
      DC.W        $ffff       ;
      DC.W        $ffff       ;
      DC.W        $ffff       ;
      DC.W        $ffff       ;
      DC.W        $ffff       ;
      DC.W        $ffff       ;
      DC.W        $ffff       ;
      DC.W        $ffff       ;
      DC.W        $ffff       ;
      DC.W        $ffff       ;
      DC.W        $ffff       ;
      DC.W        $ffff       ;
      DC.W        $ffff       ;
      DC.W        $ffff       ;

      ; Unused bits [96... 127]
      DC.W        $ffff       ;
      DC.W        $ffff       ;
      DC.W        $ffff       ;
      DC.W        $ffff       ;
      DC.W        $ffff       ;
      DC.W        $ffff       ;
      DC.W        $ffff       ;
      DC.W        $ffff       ;
      DC.W        $ffff       ;
      DC.W        $ffff       ;
      DC.W        $ffff       ;
      DC.W        $ffff       ;
      DC.W        $ffff       ;
      DC.W        $ffff       ;
      DC.W        $ffff       ;
      DC.W        $ffff       ;
      DC.W        $ffff       ;
      DC.W        $ffff       ;
      DC.W        $ffff       ;
      DC.W        $ffff       ;
      DC.W        $ffff       ;
      DC.W        $ffff       ;
      DC.W        $ffff       ;
      DC.W        $ffff       ;
      DC.W        $ffff       ;
      DC.W        $ffff       ;
      DC.W        $ffff       ;
      DC.W        $ffff       ;
      DC.W        $ffff       ;
      DC.W        $ffff       ;
      DC.W        $ffff       ;
      DC.W        $ffff       ;

      ; Reserved words [128... 271]
      DS.W        144,0  

      ; Paranoia space
      DS.W        100,0


