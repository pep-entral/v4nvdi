        include "const.s"

        XREF    struct_1
        XREF    struct_3
        XREF    vdi_escape_old

        TEXT

        XDEF    cursconf

cursconf:
        MOVE.W    (A0)+,D0
        CMP.W     #5,D0 
        BHI.B     .done 
        MOVE.B    .jump(PC,D0.W),D0 
        JSR       .jump(PC,D0.W)
.done:  RTE 

.jump:
        dc.b		.l1-.jump,.l2-.jump
        dc.b		.l3-.jump,.l5-.jump
        dc.b		.l6-.jump,.l7-.jump

.l1:    BRA       hide_cursor 

.l2:    TST.W     disab_cnt 
        BEQ.B     .l4 
        MOVE.W    #1,disab_cnt
        BRA       show_cursor 

.l3:    BSET      #0,v_stat_0.W
.l4:    RTS 

.l5:    BCLR      #0,v_stat_0.W
        RTS 

.l6:    MOVE.B    1(A0),vct_init.W 
        RTS 

.l7:    MOVEQ     #0,D0 
        MOVE.B    vct_init.W,D0
        RTS 
        
palette:
      DC.l      $FFFFFF,$FF0000
      DC.l      $00FF00,$FFFF00 
      DC.l      $0000FF,$FF00FF 
      DC.l      $00FFFF,$D5D5D5 
      DC.l      $838383,$AC0000 
      DC.l      $00AC00,$ACAC00 
      DC.l      $0000AC,$AC00AC 
      DC.l      $00ACAC,$000000 

        XDEF    curtext

curtext:
        MOVEM.L   A2-A3/D1-D3,-(A7) 
        MOVEA.L   4(A0),A3
        MOVE.W    6(A1),D3
        SUBQ.W    #1,D3 
        BMI.B     .l2 
.l1:    MOVE.W    (A3)+,D1
        MOVEA.L   con_state,A0
        JSR       (A0)
        DBF       D3,.l1
.l2:    MOVEM.L   (A7)+,A2-A3/D1-D3 
        RTS 

move_cursor:  
        BSR.B     hide_cursor 
        MOVE.W    v_cel_mx.W,D2
        TST.W     D0
        BPL.B     .l1 
        MOVEQ     #0,D0 
.l1:    CMP.W     D2,D0 
        BLE.B     .l2 
        MOVE.W    D2,D0 
.l2:    MOVE.W    v_cel_my.W,D2
        TST.W     D1
        BPL.B     .l3 
        MOVEQ     #0,D1 
.l3:    CMP.W     D2,D1 
        BLE.B     .l4 
        MOVE.W    D2,D1 
.l4:    MOVEM.W   D0-D1,v_cur_cx.W 
        MOVEA.L   v_bas_ad.W,A1 

        ;MULU      v_cel_wr.W,D1
        MULU      v_cel_ht.W,D1
        MULU      _bytes_lin.W,D1 

        ADDA.L    D1,A1 
        MULU      #24,d0 
        ADDA.W    D0,A1 
        MOVE.L    A1,v_cur_ad.W
        BRA.B     show_cursor 

hide_cursor:  
        ADDQ.W    #1,disab_cnt
        CMPI.W    #1,disab_cnt
        BNE.B     .l1 
        BCLR      #1,v_stat_0.W
        BNE.B     neg_cell 
.l1:    RTS 

show_cursor:
        CMPI.W    #1,disab_cnt
        BCS.B     .l2 
        BHI.B     .l1 
        MOVE.B    vct_init.W,v_cur_tim.W 
        BSR.B     neg_cell 
        BSET      #1,v_stat_0.W
.l1:    SUBQ.W    #1,disab_cnt
.l2:    RTS 

        XDEF      blink

blink:
        BTST      #0,v_stat_0.W
        BEQ.B     .l1 
        BCHG      #1,v_stat_0.W
        BRA.B     neg_cell 

.l1:    BSET      #1,v_stat_0.W
        BEQ.B     neg_cell 
        RTS 

neg_cell:
      MOVEM.L   A4-A5/A1/D0-D1,-(A7)
      MOVEQ     #$10,D0 
      SUB.W     v_cel_ht.W,D0
      ADD.W     D0,D0 
      MOVE.W    D0,D1 
      ADD.W     D0,D0 
      ADD.W     D1,D0 
      MOVEA.L   v_cur_ad.W,A1
      MOVEQ     #-$18,D1
      ADD.W     _bytes_lin.W,D1
      JMP       .l1(PC,D0.W)
.l1:  NOT.L     (A1)+ 
      NOT.L     (A1)+ 
      NOT.L     (A1)+ 
      NOT.L     (A1)+ 
      NOT.L     (A1)+ 
      NOT.L     (A1)+ 
      ADDA.W    D1,A1 
      NOT.L     (A1)+ 
      NOT.L     (A1)+ 
      NOT.L     (A1)+ 
      NOT.L     (A1)+ 
      NOT.L     (A1)+ 
      NOT.L     (A1)+ 
      ADDA.W    D1,A1 
      NOT.L     (A1)+ 
      NOT.L     (A1)+ 
      NOT.L     (A1)+ 
      NOT.L     (A1)+ 
      NOT.L     (A1)+ 
      NOT.L     (A1)+ 
      ADDA.W    D1,A1 
      NOT.L     (A1)+ 
      NOT.L     (A1)+ 
      NOT.L     (A1)+ 
      NOT.L     (A1)+ 
      NOT.L     (A1)+ 
      NOT.L     (A1)+ 
      ADDA.W    D1,A1 
      NOT.L     (A1)+ 
      NOT.L     (A1)+ 
      NOT.L     (A1)+ 
      NOT.L     (A1)+ 
      NOT.L     (A1)+ 
      NOT.L     (A1)+ 
      ADDA.W    D1,A1 
      NOT.L     (A1)+ 
      NOT.L     (A1)+ 
      NOT.L     (A1)+ 
      NOT.L     (A1)+ 
      NOT.L     (A1)+ 
      NOT.L     (A1)+
      ADDA.W    D1,A1 
      NOT.L     (A1)+ 
      NOT.L     (A1)+ 
      NOT.L     (A1)+ 
      NOT.L     (A1)+ 
      NOT.L     (A1)+ 
      NOT.L     (A1)+ 
      ADDA.W    D1,A1 
      NOT.L     (A1)+ 
      NOT.L     (A1)+ 
      NOT.L     (A1)+ 
      NOT.L     (A1)+ 
      NOT.L     (A1)+ 
      NOT.L     (A1)+ 
      ADDA.W    D1,A1 
      NOT.L     (A1)+ 
      NOT.L     (A1)+ 
      NOT.L     (A1)+ 
      NOT.L     (A1)+ 
      NOT.L     (A1)+ 
      NOT.L     (A1)+ 
      ADDA.W    D1,A1 
      NOT.L     (A1)+ 
      NOT.L     (A1)+ 
      NOT.L     (A1)+ 
      NOT.L     (A1)+ 
      NOT.L     (A1)+ 
      NOT.L     (A1)+ 
      ADDA.W    D1,A1 
      NOT.L     (A1)+ 
      NOT.L     (A1)+ 
      NOT.L     (A1)+ 
      NOT.L     (A1)+ 
      NOT.L     (A1)+ 
      NOT.L     (A1)+ 
      ADDA.W    D1,A1 
      NOT.L     (A1)+ 
      NOT.L     (A1)+ 
      NOT.L     (A1)+ 
      NOT.L     (A1)+ 
      NOT.L     (A1)+ 
      NOT.L     (A1)+ 
      ADDA.W    D1,A1 
      NOT.L     (A1)+ 
      NOT.L     (A1)+ 
      NOT.L     (A1)+ 
      NOT.L     (A1)+ 
      NOT.L     (A1)+ 
      NOT.L     (A1)+ 
      ADDA.W    D1,A1 
      NOT.L     (A1)+ 
      NOT.L     (A1)+ 
      NOT.L     (A1)+ 
      NOT.L     (A1)+ 
      NOT.L     (A1)+ 
      NOT.L     (A1)+ 
      ADDA.W    D1,A1 
      NOT.L     (A1)+ 
      NOT.L     (A1)+ 
      NOT.L     (A1)+ 
      NOT.L     (A1)+ 
      NOT.L     (A1)+ 
      NOT.L     (A1)+
      ADDA.W    D1,A1 
      NOT.L     (A1)+ 
      NOT.L     (A1)+ 
      NOT.L     (A1)+ 
      NOT.L     (A1)+ 
      NOT.L     (A1)+ 
      NOT.L     (A1)+ 
      MOVEM.L   (A7)+,A4-A5/A1/D0-D1
      RTS 

do_bell:
        BTST      #2,conterm.W 
        BEQ.B     .l1 
        MOVEA.L   bell_hook.W,A0 
        JMP       (A0)
.l1:    rts
    
        ; this seems to be leftover code, can probably be thrown away
        MOVEM.W   v_cur_cx.W,D0-D1  ; FIXME! wtf? This is bad.
        SUBQ.W    #1,D0 
        BRA       move_cursor 

do_tab:
        ANDI.W    #$FFF8,D0 
        ADDQ.W    #8,D0 
        BRA       move_cursor 

ascii_lf:
        PEA       show_cursor 
        BSR       hide_cursor 
        SUB.W     v_cel_my.W,D1
        BEQ       scrup 

        ;MOVE.W    v_cel_wr.W,D1
        MOVE.W    v_cel_ht.W,D1
        MULU      _bytes_lin.W,D1

        ADD.L     D1,v_cur_ad.W
        ADDQ.W    #1,v_cur_cy.W
        RTS 

ascii_cf:
        BSR       hide_cursor 
        PEA       show_cursor 
        MOVEA.L   v_cur_ad.W,A1

reset_curx:
        MULU      #24,D0
        SUBA.W    D0,A1 
        MOVE.L    A1,v_cur_ad.W
        CLR.W     v_cur_cx.W 
        RTS 

setstate_esc:
        MOVE.L    #esc_ch1,con_state
        RTS 

handle_control: 
        CMPI.W    #$1B,D1 
        BEQ.B     setstate_esc 
        SUBQ.W    #7,D1 
        SUBQ.W    #6,D1 
        BHI.B     .l1 
        MOVE.L    #normal_ascii,con_state 
        MOVE.W    .jump(PC,D1.W*2),D2 
        MOVEM.W   v_cur_cx.W,D0-D1 
        JMP       .jump(PC,D2.W)
.l1:    RTS 

        ; ascii 0-6 are already filtered out
        dc.w		do_bell-.jump		; ascii 7
        dc.w		escD-.jump		; ascii 8
        dc.w		do_tab-.jump		; ascii 9
        dc.w		ascii_lf-.jump		; ascii 10
        dc.w		ascii_lf-.jump		; ascii 11
        dc.w		ascii_lf-.jump		; ascii 12
.jump:  dc.w		ascii_cf-.jump		; ascii 13


        XDEF    con_out

con_out:  
        MOVEA.L   con_state,A0
        JMP       (A0)

normal_ascii:
        CMPI.W    #$20,D1 
        BLT.B     handle_control 

        XDEF      ascii_out

ascii_out:
        MOVEA.L   font_data,A0
        MOVEA.L   v_cur_ad.W,A1
        MOVEQ     #-32,D2 
        ADD.W     _bytes_lin.W,D2
        MOVE.B    #2,v_cur_tim.W
        BCLR      #1,v_stat_0.W
        MOVE.L    v_fnt_ad.W,D0
        BTST      #4,v_stat_0.W
        BNE       .l6 
     ;   CMP.L     font_header,D0
     ;   BNE       .l6 
        CMPI.L    #$F,v_col_bg.W 
        BNE       .l6 
        LSL.W     #4,D1 
        ADDA.W    D1,A0 
        MOVE.W    v_cel_ht.W,D1
        SUBQ.W    #1,D1 
        MOVE.L    char_table_ptr,A2
        MOVE.L    A3,-(A7)
.l1:    MOVEQ     #0,D0 
        MOVE.B    (A0)+,D0
        MULU      #24,D0
        MOVEA.L   A2,A3 
        ADDA.W    D0,A3 
        
        rept 6
        MOVE.L    (A3)+,(A1)+ 
        endr

        ADDA.W    D2,A1 
        DBF       D1,.l1
        MOVEA.L   (A7)+,A3
.l2:    MOVE.W    v_cur_cx.W,D0
        CMP.W     v_cel_mx.W,D0
        BGE.B     .l3 
        ADDI.L    #$18,v_cur_ad.W
        ADDQ.W    #1,v_cur_cx.W
        MOVEQ     #-1,D0 
        RTS 

.l3:    BTST      #3,v_stat_0.W
        BEQ.B     .l5 
        ADDQ.W    #1,disab_cnt
        EXT.L     D0
        MULU      #24,D0
        SUB.L     D0,v_cur_ad.W
        CLR.W     v_cur_cx.W 
        MOVE.W    v_cur_cy.W,D1
        PEA       .l4 
        CMP.W     v_cel_my.W,D1
        BGE       scrup 
        ADDQ.L    #4,A7 

        ;MOVE.W    v_cel_wr.W,D0
        MOVE.W    v_cel_ht.W,D0 
        MULU      _bytes_lin.W,D0

        ADD.L     D0,v_cur_ad.W
        ADDQ.W    #1,v_cur_cy.W
.l4:    SUBQ.W    #1,disab_cnt
.l5:    RTS 

.l6:    MOVEM.L   A3/D3-D4,-(A7)
        LEA       v_col_bg.W,A2
        MOVEM.W    (A2)+,D2/D3
        LEA       palette,A2
        MOVE.L    0(A2,D2.W*4),D2 
        MOVE.L    0(A2,D3.W*4),D3 
        BTST      #4,v_stat_0.W
        BEQ.B     .l7 
        EXG       D3,D2 
.l7:    MOVEA.L   D0,A0 
        ADDA.W    D1,A0 
        MOVEA.W   v_fnt_wr.W,A2
        MOVEA.W   _bytes_lin.W,A3
        LEA       -32(A3),A3
        MOVE.W    v_cel_ht.W,D1
        SUBQ.W    #1,D1 
.l8:    MOVE.B    (A0),D4 
        MOVEQ     #7,D0 
.l9:    ADD.B     D4,D4 
        BCC.B     .l10 
        MOVE.L    D3,(A1)+
        DBF       D0,.l9
        ADDA.W    A2,A0 
        ADDA.W    A3,A1 
        DBF       D1,.l8
        MOVEM.L   (A7)+,D3-D4 
        BRA       .l2 

.l10:   MOVE.L    D2,(A1)+
        DBF       D0,.l9
        ADDA.W    A2,A0 
        ADDA.W    A3,A1 
        DBF       D1,.l8
        MOVEM.L   (A7)+,A3/D3-D4
        BRA       .l2 

esc_ch1:
        CMPI.W    #'Y',D1 
        BEQ       setstate_get_row 
        MOVE.W    D1,D2 
        MOVEM.W   v_cur_cx.W,D0-D1 
        MOVEA.L   v_cur_ad.W,A1
        MOVEA.W   _bytes_lin.W,A2
        MOVE.L    #normal_ascii,con_state
        SUBI.W    #$41,D2 
        CMPI.W    #$C,D2
        BHI.B     .l1 

        ;ADD.W     D2,D2 
        MOVE.W    .jump1(PC,D2.W*2),D2 
        JMP       .jump1(PC,D2.W)

.l1:    SUBI.W    #$21,D2 
        CMPI.W    #$15,D2 
        BHI.B     .l2 
        ;ADD.W     D2,D2 
        MOVE.W    .jump2(PC,D2.W*2),D2 
        JMP       .jump2(PC,D2.W)

.l2:    RTS 

.jump1: ; jumptable for ESC-A .. ESC-L
        dc.w	escA-.jump1, escB-.jump1, escC-.jump1, escD-.jump1
        dc.w	escE-.jump1, escF-.jump1, escG-.jump1, escH-.jump1
        dc.w	escI-.jump1, escJ-.jump1, escK-.jump1, escL-.jump1
        dc.w	escM-.jump1

.jump2: ; jumptable for ESC-b .. ESC-w
        dc.w	escb-.jump2, escc-.jump2, escd-.jump2, esce-.jump2
        dc.w	escf-.jump2, escg-.jump2, esch-.jump2, esci-.jump2
        dc.w	escj-.jump2, esck-.jump2, escl-.jump2, escm-.jump2
        dc.w	escn-.jump2, esco-.jump2, escp-.jump2, escq-.jump2
        dc.w	escr-.jump2, escs-.jump2, esct-.jump2, escu-.jump2
        dc.w	escv-.jump2, escw-.jump2

escg:
esch:
esci:
escm:
escn:
escr:
escs:
esct:
escu:
escF:
escG:
        rts

escA:
        subq.w    #1,D1 
        BRA       move_cursor 

escB:
        ADDQ.W    #1,D1 
        BRA       move_cursor 
escC:
        ADDQ.W    #1,D0 
        BRA       move_cursor
escD:
        SUBQ.W    #1,D0 
        BRA       move_cursor 
escE:
        BSR       hide_cursor 
        BSR       clear_screen 
        BRA.B     cur_home 
escH:
        BSR       hide_cursor 

cur_home:
        CLR.L     v_cur_cx.W 
        MOVEA.L   v_bas_ad.W,A1 
        MOVE.L    A1,v_cur_ad.W
        BRA       show_cursor 

escI:
        PEA       show_cursor 
        BSR       hide_cursor 
        SUBQ.W    #1,D1 
        BLT       scrdn 

        ;SUBA.W    v_cel_wr.W,A1
        ;MOVE.L    A1,v_cur_ad.W

        MOVE.W    D1,v_cur_cy.W

        MOVE.W    v_cel_ht.W,D1       ;
        MULU      _bytes_lin.W,D1    ;
        SUB.W     D1,A1               ;
        MOVE.L    A1,v_cur_ad.W       ;

        RTS 

escJ:
        BSR.B     escK 
        MOVE.W    v_cur_cy.W,D1
        MOVE.W    v_cel_my.W,D2
        SUB.W     D1,D2 
        BEQ.B     .l1 
        MOVEM.L   A1-A6/D2-D7,-(A7) 
        MOVEA.L   v_bas_ad.W,A1 
        ADDQ.W    #1,D1 

        ;MULU      v_cel_wr.W,D1
        MULU      v_cel_ht.W,D1
        MULU      _bytes_lin.W,D1
        
        ADDA.L    D1,A1 
        MOVE.W    D2,D7 
        MULU      v_cel_ht.W,D7
        SUBQ.W    #1,D7 
        BRA       ca_quick2 
.l1:    RTS 

escK:   BSR       hide_cursor 
        MOVE.W    v_cel_mx.W,D2
        SUB.W     D0,D2 
        BSR       erase_line 
        BRA       show_cursor 

escL:
        PEA       show_cursor 
        BSR       hide_cursor 
        BSR       reset_curx 
        MOVEM.L   A1-A6/D2-D7,-(A7) 
        MOVE.W    v_cel_my.W,D5
        SUB.W     D1,D5 
        BEQ.B     .l1 
        MOVEA.L   v_bas_ad.W,A0 
        MOVEA.L   A0,A1 
        MOVEA.W   _bytes_lin.W,A2
        MOVEA.W   A2,A3 
        MOVE.W    v_cel_ht.W,D0
        MULU      D0,D1 
        MOVE.W    D1,D3 
        ADD.W     D0,D3 
        MOVE.W    v_cel_mx.W,D4
        LSL.W     #3,D4 
        ADDQ.W    #7,D4 
        MULU      D0,D5 
        SUBQ.W    #1,D5 
        MOVEQ     #3,D7 
        MOVEQ     #0,D0 
        MOVEQ     #0,D2 
        BSR       scroll 
        MOVEA.L   v_bas_ad.W,A1 
        MOVE.W    v_cur_cy.W,D0

        ;MULU      v_cel_wr.W,D0
        MULU      v_cel_ht.W,D0
        MULU      _bytes_lin.W,D0

        ADDA.L    D0,A1 
        BRA       ca_quick1 

.l1:    MOVEA.L   v_cur_ad.W,A1
        BRA       ca_quick1 

escM:
        PEA       show_cursor 
        BSR       hide_cursor 
        BSR       reset_curx 
        MOVEM.L   A1-A6/D2-D7,-(A7) 
        MOVE.W    v_cel_my.W,D7
        SUB.W     D1,D7 
        BEQ       ca_quick1 
        MOVE.W    v_cel_ht.W,D3
        MOVEQ     #0,D0 
        MULU      D3,D1 
        MOVEQ     #0,D2 
        ADD.W     D1,D3 
        EXG       D3,D1 
        MOVEM.W   v_cel_mx.W,D4-D5 
        ADDQ.W    #1,D4 
        LSL.W     #4,D4 
        SUBQ.W    #1,D4 
        MULU      v_cel_ht.W,D5
        SUBQ.W    #1,D5 
        SUB.W     D1,D5 
        BRA       do_scrup 

setstate_get_row:
        MOVE.L    #get_row,con_state
        RTS 

get_row:
        SUBI.W    #$20,D1 
        MOVE.W    v_cur_cx.W,D0
        MOVE.L    #get_column,con_state
        BRA       move_cursor 

get_column:
        SUBI.W    #$20,D1 
        MOVE.W    D1,D0 
        MOVE.W    v_cur_cy.W,D1
        MOVE.L    #normal_ascii,con_state
        BRA       move_cursor 




escb:
.setstate_set_fg:
        MOVE.L    #.set_fg,con_state
        RTS 

.set_fg:
        LEA       v_col_fg.W,A1
        BRA.B     set_color
                
escc:
.setstate_set_bg:
        MOVE.L    #.set_bg,con_state
        RTS 

.set_bg:
        LEA       v_col_bg.W,A1
      ; BRA.B     set_color 


set_color:
        MOVEQ     #$F,D0
        AND.W     D0,D1 
        MOVE.W    D1,(A1) 
        MOVE.L    #normal_ascii,con_state
        RTS 




escd:
        BSR.B     esco 
        MOVE.W    v_cur_cy.W,D1
        BEQ.B     .l1 
        MOVEM.L   A1-A6/D2-D7,-(A7) 
        MULU      v_cel_ht.W,D1
        MOVE.W    D1,D7 
        SUBQ.W    #1,D7 
        MOVEA.L   v_bas_ad.W,A1 
        BRA       ca_quick2 
.l1:    RTS 

esce:
        TST.W     disab_cnt 
        BEQ.B     .l1 
        MOVE.W    #1,disab_cnt
        BRA       show_cursor 
.l1:    RTS 

escf:
        BRA       hide_cursor 

escj:
        BSET      #5,v_stat_0.W
        MOVE.L    v_cur_cx.W,sav_cx.W 
        RTS 

esck:
        MOVEM.W   sav_cx.W,D0-D1 
        BCLR      #5,v_stat_0.W
        BNE       move_cursor 
        MOVEQ     #0,D0 
        MOVEQ     #0,D1 
        BRA       move_cursor 

escl:
        BSR       hide_cursor 
        BSR       reset_curx 
        BSR       clear_area 
        BRA       show_cursor 

esco:   MOVE.W    D0,D2 
        SUBQ.W    #1,D2 
        BMI.B     .l1 
        MOVEA.L   v_bas_ad.W,A1 

        ;MULU      v_cel_wr.W,D1
        MULU      v_cel_ht.W,D1 
        MULU      _bytes_lin.W,D1

        ADDA.L    D1,A1 
        BRA       erase_line 
.l1:    RTS 

escp:   BSET      #4,v_stat_0.W
        RTS 

escq:   BCLR      #4,v_stat_0.W
        RTS 

escv:   BSET      #3,v_stat_0.W
        RTS 

escw:   BCLR      #3,v_stat_0.W
        RTS 

scrup:  MOVEM.L   A1-A6/D2-D7,-(A7) 
        MOVEQ     #0,D0 
        MOVE.W    v_cel_ht.W,D1
        MOVEQ     #0,D2 
        MOVEQ     #0,D3 
        MOVEM.W   v_cel_mx.W,D4-D5 
        ADDQ.W    #1,D4 
        LSL.W     #3,D4 
        SUBQ.W    #1,D4 
        MULU      v_cel_ht.W,D5
        SUBQ.W    #1,D5 
        
do_scrup:
        MOVEQ     #3,D7 
        MOVEA.L   v_bas_ad.W,A0 
        MOVEA.L   A0,A1 
        MOVEA.W   _bytes_lin.W,A2
        MOVEA.W   A2,A3 
        BSR       scroll 
        MOVEA.L   v_bas_ad.W,A1 
        MOVE.W    v_cel_my.W,D0

        ;MULU      v_cel_wr.W,D0
        MULU      v_cel_ht.W,D0 
        MULU      _bytes_lin.W,D0

        ADDA.L    D0,A1 
        BRA.B     ca_quick1 

scrdn:  MOVEM.L   A1-A6/D2-D7,-(A7) 
        MOVEQ     #0,D0 
        MOVEQ     #0,D1 
        MOVEQ     #0,D2 
        MOVE.W    v_cel_ht.W,D3
        MOVEM.W   v_cel_mx.W,D4-D5 
        ADDQ.W    #1,D4 
        ADDQ.W    #1,D5 
        LSL.W     #3,D4 
        SUBQ.W    #1,D4 
        MULU      D3,D5 
        SUBQ.W    #1,D5 
        MOVEQ     #3,D7 
        MOVEA.L   v_bas_ad.W,A0 
        MOVEA.L   A0,A1 
        MOVEA.W   _bytes_lin.W,A2
        MOVEA.W   A2,A3 
        BSR       scroll 
        MOVEA.L   v_bas_ad.W,A1 
        BRA.B     ca_quick1 

clear_screen: 
        MOVEM.L   A1-A6/D2-D7,-(A7) 
        MOVE.W    v_cel_my.W,D7
        ADDQ.W    #1,D7 
        MULU      v_cel_ht.W,D7
        SUBQ.W    #1,D7 
        MOVEA.L   v_bas_ad.W,A1 
        BRA.B     ca_quick2 

clear_area:
        MOVEM.L   A1-A6/D2-D7,-(A7) 
        
ca_quick1:
        MOVE.W    v_cel_ht.W,D7
        SUBQ.W    #1,D7 
        
ca_quick2:
        MOVE.W    v_col_bg.W,D6
        LEA       palette,A2
        MOVE.L    (A2,D6.W*4),D6 
        MOVE.W    v_cel_mx.W,D4
        MOVE.W    _bytes_lin.W,D5
        MOVE.W    D4,D2 
        ADDQ.W    #1,D2 
        MULU      #24,D2
        SUB.W     D2,D5 
.l1:    MOVE.W    D4,D2 
.l2:    MOVE.L    D6,(A1)+
        MOVE.L    D6,(A1)+
        MOVE.L    D6,(A1)+
        MOVE.L    D6,(A1)+
        MOVE.L    D6,(A1)+
        MOVE.L    D6,(A1)+
        DBF       D2,.l2
        ADDA.W    D5,A1 
        DBF       D7,.l1
        MOVEM.L   (A7)+,A1-A6/D2-D7 
        RTS 

erase_line:                    
        MOVE.L    D3,-(A7)
        MOVE.W    v_col_bg.W,D3
        LEA       palette,A0
        MOVE.L    0(A0,D3.W*4),D3 
        MOVE.W    v_cel_ht.W,D1
        SUBQ.W    #1,D1 
        MOVE.W    D2,D0 
        ADDQ.W    #1,D0 
        MULU      #24,D0
        MOVEA.W   _bytes_lin.W,A0
        SUBA.W    D0,A0 
.l1:    MOVE.W    D2,D0 
.l2:    MOVE.L    D3,(A1)+
        MOVE.L    D3,(A1)+
        MOVE.L    D3,(A1)+
        MOVE.L    D3,(A1)+
        MOVE.L    D3,(A1)+
        MOVE.L    D3,(A1)+
        DBF       D0,.l2
        ADDA.W    A0,A1 
        DBF       D1,.l1
        MOVE.L    (A7)+,D3
        RTS 

scroll: MOVEA.L   struct_3,A6
        MOVE.W    D7,494(A6)
        MOVE.L    A0,450(A6)
        MOVE.L    A1,470(A6)
        MOVE.W    A2,454(A6)
        MOVE.W    A3,474(A6)
        MOVE.W    #$F,456(A6) 
        MOVE.W    #$F,476(A6) 
        MOVE.W    D4,D6 
        MOVE.W    D5,D7 

        MOVEA.L   524(a6),A0
        JMP       (A0)

        XDEF    init_char

init_char: 
        MOVEM.L   A0-A1/D0-D2,-(A7) 
        MOVE.L    #char_table_buf,D0
        add.l     #15,d0
        and.b     #$f0,d0
        move.l    d0,a0
        move.l    a0,char_table_ptr
        MOVEQ     #0,D0 
.l1:    MOVE.W    D0,D1 
        MOVEQ     #7,D2 
.l2:    CLR.L     (A0)+ 
        ADD.B     D1,D1 
        BCS.B     .l3 
        NOT.L     -2(A0)
.l3:    DBF       D2,.l2
        ADDQ.W    #1,D0 
        CMP.W     #$100,D0
        BLT.B     .l1 
        MOVEM.L   (A7)+,A0-A1/D0-D2 
        RTS 

        XDEF    init_console

init_console:
        MOVE.L    A0,-(A7)
        MOVEA.L   struct_1,A0
        MOVEA.L   36(A0),A0 
        MOVE.L    6(A0),font_header 
        MOVE.L    10(A0),font_data
        MOVE.L    #normal_ascii,con_state
        MOVEA.L   (A7)+,A0
        RTS 

        XDEF    vdi_escape

vdi_escape:
        MOVEA.L   (A0),A1 
        MOVE.W    10(A1),D0 
        CMPI.W    #$C,D0
        BEQ       curtext 
        CMP.W     #$F,D0
        BHI.B     .l1 
        MOVEM.L   A2-A5/D1-D7,-(A7) 
        MOVEM.L   4(A0),A2-A5 
      ;  ADD.W     D0,D0 
        MOVE.W    esc_lookup(PC,D0.W*2),D2 
        MOVEA.L   A2,A5 
        MOVEA.L   A1,A0 
        MOVEM.W   v_cur_cx.W,D0-D1 
        MOVEA.L   v_cur_ad.W,A1
        MOVEA.W   _bytes_lin.W,A2
        JSR       esc_lookup(PC,D2.W)
        MOVEM.L   (A7)+,A2-A5/D1-D7 
        RTS 

.l1:    MOVEA.L   vdi_escape_old,A1
        JMP       (A1)

esc_lookup:
        dc.w	invalid-esc_lookup,     chcells-esc_lookup
        dc.w    exit_alpha-esc_lookup,  enter_alpha-esc_lookup
        dc.w	escA-esc_lookup,        escB-esc_lookup
        dc.w    escC-esc_lookup,        escD-esc_lookup
        dc.w	escH-esc_lookup,        escJ-esc_lookup
        dc.w    escK-esc_lookup,        set_curadr-esc_lookup 
        dc.w	curtext-esc_lookup,     escp-esc_lookup
        dc.w    escq-esc_lookup,        get_curadr-esc_lookup

invalid: 
        rts

chcells:
        MOVE.L    v_cel_mx.W,D3
        ADDI.L    #$10001,D3
        SWAP      D3
        MOVE.L    D3,(A4) 
        MOVE.W    #2,8(A0)
        RTS 



exit_alpha:
        ADDQ.W    #1,disab_cnt
        BCLR      #1,v_stat_0.W
        BRA       clear_screen 

enter_alpha:
        CLR.L     v_cur_cx.W 
        MOVE.L    v_bas_ad.W,v_cur_ad.W
        MOVE.L    #normal_ascii,con_state
        BSR       clear_screen 
        BCLR      #1,v_stat_0.W
        MOVE.W    #1,disab_cnt
        BRA       show_cursor 



set_curadr:
        MOVE.W    (A5)+,D1
        MOVE.W    (A5)+,D0
        SUBQ.W    #1,D0 
        SUBQ.W    #1,D1 
        BRA       move_cursor 

get_curadr:
        ADDQ.W    #1,D0 
        ADDQ.W    #1,D1 
        MOVE.W    D1,(A4)+
        MOVE.W    D0,(A4)+
        MOVE.W    #2,8(A0)
        RTS 


        EVEN
    
        BSS
        
        ; PUBLIC
        
        XDEF    con_state
        XDEF    disab_cnt
        
disab_cnt:      DS.W     1 
con_state:      DS.L     1  

        ; PRIVATE

font_header:    DS.L     1 
font_data:      DS.L     1 

char_table_ptr: DS.L     1
char_table_buf: DS.L     256*8
                DS.B     16 /* extra space for alignment */
        END

