#ifndef _GLUE_H_
#define _GLUE_H_

#include <stdint.h>
#include "nvdidefs.h"

static inline void* call_purec_a (void* func, void* prm1)
{
	register void* __ret __asm__("d0");	
	register void* _prm1 __asm__("a0") = prm1;

	__asm__ volatile (
		"jsr (%1)\n\t"
		: "=r"(__ret)
		: "r"(func),"r"(_prm1)
		: __CLOBBER_RETURN("d0") "d1","d2","a1","a2","memory","cc"
	);

	return __ret;
}

static inline void* call_purec_aa (void* func, void* prm1, void* prm2)
{
	register void* __ret __asm__("d0");	
	register void* _prm1 __asm__("a0") = prm1;
	register void* _prm2 __asm__("a1") = prm2;

	__asm__ volatile (
		"jsr (%1)\n\t"
		: "=r"(__ret)
		: "r"(func),"r"(_prm1),"r"(_prm2)
		: __CLOBBER_RETURN("d0") "d1","d2","a2","memory","cc"
	);

	return __ret;
}

static inline int32_t call_purec_dd (void* func, int32_t prm1, int32_t prm2)
{
	register uint32_t _prm1 __asm__("d0") = prm1;
	register uint32_t _prm2 __asm__("d1") = prm2;

	__asm__ volatile (
		"jsr (%0)\n\t"
		:
		: "r"(func),"r"(_prm1),"r"(_prm2)
		: __CLOBBER_RETURN("d0") "a0","a1","d2","a2","memory","cc"
	);

	return _prm1;
}

#define nvdi_cookie_init(name, val) 	((int32_t)call_purec_dd(nvdi->init_cookie,(int32_t)(name), (int32_t)(val)))
#define nvdi_cookie_reset(name) 		((void)call_purec_d(nvdi->reset_cookie,(int32_t)(name)))
#define nvdi_cookie_search(name) 		((int32_t)call_purec_d(nvdi->search_cookie,(int32_t)(name)))
#define nvdi_load_prg(filename) 		((void*)call_purec_a(nvdi->load_prg, (void*)(filename)))
#define nvdi_malloc_sys(filename) 		((void*)call_purec_d(nvdi->Malloc_sys, (int32_t)(size)))
#define nvdi_mfree_sys(ptr)				((void)call_purec_a(nvdi->Mfree_sys, (void*)(ptr)))

extern driver_header_t screen_sys_header;
extern void reset(struct _nvdi_struct_t *);

extern int16_t modecode_old;

#endif
