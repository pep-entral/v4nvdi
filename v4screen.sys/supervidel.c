#include <stdint.h>
#include <stddef.h>
#include <mint/linea.h>
#include <mint/falcon.h>
#include <mint/sysvars.h>
#include "nvdidefs.h"
#include "screen.h"

static int16_t modecode_old;

const char* screen_init(vdipb_t* pb, nvdi_struct_t* nvdi)
{
	nvdi->xbios_tab[1] = nvdi->xbios_tab[0];

	modecode_old = VsetMode(-1);

	if(pb->intin[0] >= 2 && pb->intin[0] <= 10)
		VsetScreen(NULL, NULL, pb->intin[0] - 2, pb->ptsout[0]);

	switch(pb->ptsout[0] & NUMCOLS)
	{
		case BPS1:	return "NVDIDRV1.SYS";
		case BPS2:	return "NVDIDRV2.SYS";
		case BPS4:	return "NVDIDRV4.SYS";
		case BPS8:	return "NVDIDRV8.SYS";
		case BPS16:	return "NVDIDRVH.SYS";
		case BPS32:	return "SV_BPS32.SYS";
		case BPS24:	return "SV_BPS24.SYS";
		case BPS8C:	return "SV_BPS8C.SYS";
	}

	return NULL;
}

void screen_post_init(nvdi_struct_t* nvdi)
{
}

void screen_pre_exit(nvdi_struct_t* nvdi)
{
}

void screen_exit(void)
{
	if(modecode_old != -1)
	{
		VsetScreen(NULL, NULL, 3, modecode_old);
		modecode_old = -1;
	}	
}
