#ifndef _SCREEN_H_
#define _SCREEN_H_

#include "nvdidefs.h"

#ifndef BPS24
#define BPS24 (6)
#endif

#ifndef BPS8C
#define BPS8C (7)
#endif

#if defined(PISTORM)
#include "pistorm.h"
#elif defined(VAMPIRE)
#include "vampire.h"
#elif defined(SUPERVIDEL)
#include "supervidel.h"
#else
#error Please define either VAMPIRE or SUPERVIDEL
#endif

extern const char* screen_init(vdipb_t* pb, nvdi_struct_t* nvdi);
extern void screen_post_init(nvdi_struct_t* nvdi);
extern void screen_pre_exit(nvdi_struct_t* nvdi);
extern void screen_exit(void);

#endif
