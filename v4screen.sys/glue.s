		.extern	_screen_sys_name
		.globl	_screen_sys_header

		bra.s	dummy
_screen_sys_header:
		dc.b	'N','V','D','I','D','R','V',0 
		dc.w	0x0500		|; version
		dc.w	0x0080		|; header_len
		dc.w	0x0001		|; type
		
		dc.l	init
		dc.l	_reset
		dc.l	wk_init
		dc.l	wk_reset
		dc.l	opnwkinfo
		dc.l	extndinfo
		dc.l	scrninfo

		dc.l	_screen_sys_name

		dc.l	0		|; offset_hdr
		dc.l	0		|; o52
		dc.l	0,0		|; res2[2]

		dc.l	0		|; colors
		dc.w	0		|; planes
		dc.w	0		|; format
		dc.w	0		|; flags
		dc.w	0,0,0		|; reserved[3]
wk_init:
wk_reset:
opnwkinfo:
extndinfo:
scrninfo:
dummy:		rts


		.globl _memcpy
_memcpy:	move.l 12(sp),d0
		jbeq 2$

		move.l 4(sp),a0
		move.l 8(sp),a1
		subq.l #1,d0
1$:		move.b (a1)+,(a0)+
		dbra d0,1$

2$:		rts


		.extern	_screen_sys_init
init:		movem.l	d1-d2/a0-a2,-(sp)

		move.l a1,-(sp)		|; DRIVER struct
		move.l a0,-(sp)		|; NVDI struct
		move.l a4,-(sp)
		dc.w 0xa000
		move.l d0,___aline
		jsr _screen_sys_init	
		lea.l 12(sp),sp

		movem.l	(sp)+,d1-d2/a0-a2
		rts


		.extern	_screen_sys_reset
		.globl _reset
_reset:		movem.l d1/a0-a1,-(sp)

		move.l a1,-(sp)		|; DRIVER struct 
		move.l a0,-(sp)		|; NVDI struct 
		jsr _screen_sys_reset
		addq.w #8,sp

		movem.l (sp)+,d1/a0-a1
		rts


		.globl ___aline
___aline:	dc.l 0

_modecode_old:	.globl _modecode_old
		dc.w -1

		.end
