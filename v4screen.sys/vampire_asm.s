	.equ SPRITE_POSX,	0xdff1d0	|; [16-bits] Mouse Sprite X
	.equ SPRITE_POSY,	0xdff1d2	|; [16-bits] Mouse Sprite Y
	.equ SPRITE_CLUT,	0xdff3a0	|; [16-bits] Mouse Sprite CLUT[4]
	.equ SPRITE_DATA,	0xdff800	|; [16-bits] Mouse Sprite DATA[16*16]
	.equ VIDEO_GET_PHYS,	0xdfe1ec 	|; Read framebuffer addr
	.equ VIDEO_MAXH,	16384		|; Maximum Horizontal Size
	.equ VIDEO_MAXV,	16384		|; Maximum Vertical Size
	.equ SPRITE_DELTAX,	16		|; Mouse Sprite Start X
	.equ SPRITE_DELTAY,	8		|; Mouse Sprite Start Y
	.equ _v_bas_ad,		0x44e 		|; Logical screen base

	.text

	.globl _sprite_init
_sprite_init:
	move.w #0x0000,SPRITE_CLUT + 0
	move.w #0x0fff,SPRITE_CLUT + 2
	move.w #0x0000,SPRITE_CLUT + 4
	move.w #0x0000,SPRITE_CLUT + 6
	jbra hide

	.globl _sprite_exit
_sprite_exit:
	|;jbra hide

hide:	move.w #VIDEO_MAXH,SPRITE_POSX
    	move.w #VIDEO_MAXV,SPRITE_POSY
    	rts

	.globl _sprite_draw
_sprite_undraw:
	tst.w	frames
	beq.s	hide
	sub.w	#1,frames
	rts

	| d0 :	GCURX
	| d1 :	GCURY
	| a0 :	[0]hotspotx.w
	|	[2]hotspoty.w
	|	[4]planes.w
	|	[6]bgcolor.w
	|	[8]fgcolor.w
	|	[10]data+mask[16]


	.globl _sprite_undraw
_sprite_draw:
	move.w #10,frames

	move.l	VIDEO_GET_PHYS,a1
	cmp.l 	_v_bas_ad,a1
	bne.s	hide

draw:	sub.w	(a0)+,d0	| adjust for hotspot.x
	sub.w	(a0)+,d1	| ajudst for hotspot.y
	add.w	#SPRITE_DELTAX,d0
	add.w	#SPRITE_DELTAY,d1

	tst.w	(a0)+		| ignore planes (always 1)
	tst.w	(a0)+		| ignore fg
	tst.w	(a0)+		| ignore bg

	lea.l	SPRITE_DATA,a1
	.rept 16
	move.l	(a0)+,(a1)+
	.endr

	move.w	d0,SPRITE_POSX
	move.w	d1,SPRITE_POSY
	rts

	.data

	.globl _sprite_draw_old
_sprite_draw_old:
	dc.l 0
	.globl _sprite_undraw_old
_sprite_undraw_old:
	dc.l 0

	.bss
frames:	ds.w 1

	.end
