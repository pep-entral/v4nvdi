#include <stdint.h>
#include <stddef.h>
#include <mint/linea.h>
#include <mint/falcon.h>
#include <mint/sysvars.h>
#include "nvdidefs.h"
#include "screen.h"

extern void sprite_init(void);
extern void sprite_exit(void);
extern void sprite_draw(void);
extern void sprite_undraw(void);
extern void (*sprite_draw_old)(void);
extern void (*sprite_undraw_old)(void);

static int16_t modecode_old;

const char* screen_init(vdipb_t* pb, nvdi_struct_t* nvdi)
{
	nvdi->xbios_tab[1] = nvdi->xbios_tab[0];

	modecode_old = VsetMode(-1);

	if(pb->intin[0] >= 2 && pb->intin[0] <= 10)
		VsetScreen(-1, -1, pb->intin[0] - 2, pb->ptsout[0]);

	switch(VPLANES)
	{
		default:
		case 1:		return "NVDIDRV1.SYS";
		case 2:		return "NVDIDRV2.SYS";
		case 4:		return "NVDIDRV4.SYS";
		case 8:		if(pb->ptsout[0] & 0x4000)
					return ((pb->ptsout[0] &  0x00ff) == 0x0B) ? "NVDIDRV8.SYS" : "V4_BPS8C.SYS";
				else
					return ((pb->ptsout[0] & NUMCOLS) == BPS8) ? "NVDIDRV8.SYS" : "V4_BPS8C.SYS";
				break;
		case 16:	return "NVDIDRVH.SYS";
		case 24:	return "V4_BPS24.SYS";
		case 32:	return "V4_BPS32.SYS";
	}

	return NULL;
}

void screen_post_init(nvdi_struct_t* nvdi)
{
	if(!sprite_draw_old)
	{
		sprite_draw_old = nvdi->mouse_tab[1];
		sprite_undraw_old = nvdi->mouse_tab[2];
	}

	if(V_Y_MAX >= 400)
	{
		nvdi->mouse_tab[1] = sprite_draw;
		nvdi->mouse_tab[2] = sprite_undraw;
	}
	else
	{
		nvdi->mouse_tab[1] = sprite_draw_old;
		nvdi->mouse_tab[2] = sprite_undraw_old;
	}

	sprite_init();
}

void screen_pre_exit(nvdi_struct_t* nvdi)
{
	if(sprite_draw_old)
	{
		nvdi->mouse_tab[1] = sprite_draw_old;
		nvdi->mouse_tab[2] = sprite_undraw_old;
		sprite_exit();
	}
}

void screen_exit(void)
{
	if(modecode_old != -1)
	{
		VsetScreen(-1, -1, 3, modecode_old);
		modecode_old = -1;
	}
}
