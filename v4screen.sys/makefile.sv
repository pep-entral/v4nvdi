NAME		= svscreen.sys

CC		= m68k-atari-mint-gcc
AS		= m68k-atari-mint-as
LD		= vlink
CFLAGS		= -Os -fomit-frame-pointer -fno-defer-pop -m68040
ASFLAGS		=
LDFLAGS		= -nostdlib -EB -bataritos -tos-flags 0x07 -tos-global
DEFINES		= -UDEBUG -DSUPERVIDEL

INCLUDES	= -I.
LIBS		=
          
COBJECTS	= ./screen.o ./supervidel.o
AOBJECTS	= ./glue.o 			


all: $(NAME)

$(NAME): $(COBJECTS) $(AOBJECTS)
	$(LD) $(LDFLAGS) -s -o $(NAME) $(AOBJECTS) $(COBJECTS) $(LIBS)

.c.o:
	$(CC) -c $(CFLAGS) $(INCLUDES) $(DEFINES) $< -o $@

clean:
	rm -f $(COBJECTS) $(AOBJECTS) $(NAME)
