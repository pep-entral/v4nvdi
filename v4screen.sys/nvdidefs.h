#ifndef _NVDIDEFS_H_
#define _NVDIDEFS_H_

#include <stdint.h>
#include <stdbool.h>

typedef struct vdipb_s
{
	int16_t       *control;
	const int16_t *intin;
	const int16_t *ptsin;
	int16_t       *intout;
	int16_t       *ptsout;
} vdipb_t;

typedef struct _organisation_t
{
	uint32_t colors;						/* number of simultaneous color */
	int16_t  planes;						/* number of planes */
	int16_t  format;						/* format type (0-2) */
	int16_t  flags;							/* format flags */
	int16_t  reserved[3];
} organisation_t;

typedef struct _driver_t
{
	/*  0 */ struct _driver_t *next;		/* next element in linked list */
	/*  4 */ struct _device_driver_t *code;	/* pointer to driver header */
	/*  8 */ int32_t wk_len;				/* length of workstation */
	/* 12 */ int16_t used;					/* how many times this driver is used */
	/* 14 */ organisation_t info;			/* bitmap organisation */
	/* 30 */ char file_name[16];			/* file name */
	/* 46 */ uint32_t file_size;			/* file size */
	/* 50 */ const char *file_path;			/* path for driver */
	/* 54 */ 
} driver_t;

typedef struct _device_driver_t
{
	/*  0 */ char name[9];					/* filename, without extension */
	/*  9 */ uint8_t status;				/* driver type, (GDOS-, ATARI-VDI or NVDI- driver) */
	/* 10 */ int16_t use;					/* how many times this driver is used */
	/* 12 */ struct _driver_header_t *addr;	/* pointer to driver header or NULL */
	/* 16 */ int32_t wk_len;				/* length of workstation for driver */
	/* 20 */ driver_t *offscreen;			/* pointer to offscreen driver being used */
	/* 24 */ int16_t open_hdl;				/* handle that was used to open the device */
	         int16_t res26;
	         int16_t res28;
	         int16_t res30;
} device_driver_t;

typedef struct wk_s
{
	/*   0 */ void *disp_addr1;		/* pointer to VDI dispatcher; called from VDI trap entry */
	/*   4 */ void *disp_addr2;		/* pointer to actual driver-dispatcher */
	/*   8 */ int16_t wk_handle;	/* workstation handle */
	/*  10 */ int16_t driver_id;	/* device ID */

/* coordinate system */
	/*  12 */ int16_t pixel_width;	/* width of a pixel in micro meter */
	/*  14 */ int16_t pixel_height;	/* height of a pixel in micro meter */
	/*  16 */ int16_t res_x;		/* raster width -1 */
	/*  18 */ int16_t res_y;		/* raster height -1 */
	/*  20 */ int16_t colors;		/* number of pens -1 */
	/*  22 */ int16_t res_ratio;	/* aspect ratio: <0: vertical shrink ; >0 vertical zoom */
	/*  24 */ char driver_type;     /* driver type (NVDI or GDOS) */
	          char free25;
	/*  26 */ int16_t driver_device;  /* device (screen, printer, metafile or memory) */
	/*  28 */ int16_t free28;

/* driver specific data */
	/*  30 */ char input_mode;			/* modi of input devices */
	          char free31;
	/*  32 */ void *buffer_addr;		/* pointer to tmp buffer */
	/*  36 */ int32_t buffer_len;		/* length of tmp buffer in bytes */
	/*  40 */ int32_t bez_buffer;		/* bezier buffer from v_set_app_buf */
	/*  44 */ int32_t bez_buf_len;		/* length of bezier buffer */

/* arrays for input coordinates */
	/*  48 */ void *gdos_buffer;    /* buffer for gdos driver */

/* clip limits */
	/*  52 */ int16_t clip_xmin;      /* minimum - x */
	/*  54 */ int16_t clip_ymin;      /* minimum - y */
	/*  56 */ int16_t clip_xmax;      /* maximum - x */
	/*  58 */ int16_t clip_ymax;      /* maximum - y */
	/*  60 */ int16_t wr_mode;        /* graphic mode */
	/*  62 */ int16_t bez_on;         /* beziers on? */
	/*  64 */ int16_t bez_qual;       /* bezier quality from 0-5 */
	/*  66 */ int16_t free66;
	/*  68 */ int16_t free68;
	/*  70 */ int16_t l_color;        /* line color */
	/*  72 */ int16_t l_width;        /* line width */
	/*  74 */ int16_t l_start;        /* line start */
	/*  76 */ int16_t l_end;          /* line end */
	/*  78 */ int16_t l_lastpix;      /* 1 = do not set last point */
	/*  80 */ int16_t l_style;        /* line style */
	/*  82 */ int16_t l_styles[6];    /* line patterns */
	/*  94 */ int16_t l_udstyle;      /* user defined style */
	/*  96 */ int16_t free96;
	/*  98 */ int16_t free98;

/* text handling */
	/* 100 */ int16_t t_color;        /* text color */
	/* 102 */ int16_t free102;
	/* 104 */ char free104;
	/* 105 */ char t_mapping;	    /* 0: use direct index 1: use t_asc_map */
	/* 106 */ int16_t t_first_ade;    /* code of first character */
	/* 108 */ int16_t t_ades;         /* number of characters -1 */
	/* 110 */ int16_t t_space_index;  /* index for a space (delimiter for v_justified) */
	/* 112 */ int16_t t_unknown_index; /* index for a unknown character */
	/* 114 */ char t_prop;			/* 1: font is proportional */
	/* 115 */ char t_grow;			/* -1: grow +1: shrink (bitmap-fonts only) */
	/* 116 */ int16_t t_no_kern;		/* number of kerning pairs <0: kerning disabled */
	/* 118 */ int16_t t_no_track;		/* number of track kern pairs */
	/* 120 */ int16_t t_hor;			/* horizontal alignment */
	/* 122 */ int16_t t_ver;			/* verical alignment */
	/* 124 */ int16_t t_base;			/* topline<->baseline */
	/* 126 */ int16_t t_half;			/* topline<->halfline */
	/* 128 */ int16_t t_descent;		/* topline<->descent line */
	/* 130 */ int16_t t_bottom;		/* topline<->bottom line */
	/* 132 */ int16_t t_ascent;		/* topline<->ascent line */
	/* 134 */ int16_t t_top;			/* topline<->topline */
	/* 136 */ int16_t free136;
	/* 138 */ int16_t free138;
	/* 140 */ int16_t t_left_off;		/* left offset for italic */
	/* 142 */ int16_t t_whole_off;	/* total widening for italic */
	/* 144 */ int16_t t_thicken;		/* thickening for bold */
	/* 146 */ int16_t t_uline;		/* thickness of underline */
	/* 148 */ int16_t t_ulpos;		/* distance of underline to topline */
	/* 150 */ int16_t t_width;		/* text width */
	/* 152 */ int16_t t_height;		/* text height */
	/* 154 */ int16_t t_cwidth;		/* cell width */
	/* 156 */ int16_t t_cheight;		/* cell height */
	/* 158 */ int16_t t_point_last;	/* last used point size */
	/* 160 */ int32_t t_scale_width;   /* width in 1/65536 pixel (relative) for character generation */
	/* 164 */ int32_t t_scale_height;  /* height in 1/65536 pixel for character generation */
	/* 168 */ int16_t t_rotation;		/* text rotation */
	/* 170 */ int16_t t_skew;			/* counter clockwise, in 1/10 degree */
	/* 172 */ int16_t t_effects;		/* text effects */
	/* 174 */ int16_t t_light_pct;	/* grey value */
	/* 176 */ int16_t *t_light_fill;	/* pointer to grey image for light text */
	/* 180 */ int16_t free180;
	/* 182 */ int16_t free182;
	/* 184 */ int16_t free184;
	/* 186 */ int16_t free186;
	/* 188 */ int16_t free188;

/* pattern handling */
	/* 190 */ int16_t f_color;		/* fill color */
	/* 192 */ int16_t f_interior;		/* fill interior */
	/* 194 */ int16_t f_style;		/* fill style */
	/* 196 */ int16_t f_perimeter;	/* flag for rectangle outline */
	/* 198 */ const int16_t *f_pointer; /* pointer to current fill pattern */
	/* 202 */ int16_t f_planes;		/* number of planes of pattern */
	/* 204 */ const int16_t *f_fill0;
	/* 208 */ const int16_t *f_fill1;
	/* 212 */ const int16_t *f_fill2;
	/* 216 */ const int16_t *f_fill3;
	/* 220 */ const int16_t *f_spointer; /* pointer to user defined fill pattern */
	/* 224 */ int16_t f_splanes;		/* number of planes of user defined pattern */
	/* 226 */ int16_t free226;
	/* 228 */ int16_t free228;

/* marker handling */
	/* 230 */ int16_t m_color;		/* marker color */
	/* 232 */ int16_t m_type;			/* marker type */
	/* 234 */ int16_t m_width;		/* marker width */
	/* 236 */ int16_t m_height;		/* marker height */
	/* 238 */ void *m_data;			/* pointer to marker data */
	/* 242 */ int16_t free242;
	/* 244 */ int16_t free244;
	/* 246 */ int16_t free246;
	/* 248 */ int16_t free248;
	/* 250 */ int16_t t_number;		/* font number */
	/* 252 */ char t_font_type;     /* type of font */
	          char free253;
	/* 254 */ int16_t t_bitmap_gdos;  /* 1: bitmap fonts where embedded using GDOS */
	/* 256 */ void *t_bitmap_fonts; /* pointer to more bitmap fonts */
	/* 260 */ void *t_res_ptr1;     /* reserved */
	/* 264 */ void *t_res_ptr2;     /* reserved */
	/* 268 */ int16_t t_res_xyz1;     /* reserved */

/* pointer for vector fonts */
	/* 270 */ void *t_pointer;		/* pointer to font */
	/* 274 */ void *t_fonthdr;		/* pointer to current font */
	/* 278 */ void *t_offtab;		/* pointer to character offsets */
	/* 282 */ void *t_image;        /* pointer to font data */
	/* 286 */ int16_t t_iwidth;       /* width of font data in bytes */
	/* 288 */ int16_t t_iheight;      /* height of font data in lines */

/* temporary data for bitmap text */
	/* 290 */ int16_t t_eff_thicken;  /* widening from effects */
	/* 292 */ int16_t t_act_line;     /* starting line number in text buffer */
	/* 294 */ int16_t t_add_length;   /* additional length for v_justified */
	/* 296 */ int16_t t_space_kind;   /* -1: per-character spacing */

#define t_FONT_ptr t_pointer		/* pointer to current FONT structure */
#define t_asc_map t_fonthdr         /* pointer to table ascii -> index */
#define t_BAT_ptr t_offtab          /* pointer to attribute table */
#define t_bin_table t_image         /* pointer to fast access map */

	/* 298 */ int16_t free298;

/* dimensions for vector fonts */
	/* 300 */ int32_t t_width32;		/* width in 1/65536 pixel (relative value) */
	/* 304 */ int32_t t_height32;		/* height in 1/65536 pixel */
	/* 308 */ int32_t t_point_width;	/* width in 1/65536 pixel */
	/* 312 */ int32_t t_point_height;  /* height in 1/65536 pixel */
	/* 316 */ int16_t t_track_index;  /* number of track index */
	/* 318 */ int32_t t_track_offset;  /* offset between characters in 1/65536 pixel */
	/* 322 */ int32_t t_left_off32;    /* left offset for italic */
	/* 326 */ int32_t t_whole_off32;   /* total widening for italic */
	/* 330 */ int32_t t_thicken32;     /* thickening for bold */
	          int16_t free334;
	          int16_t free336;
	          int16_t free338;
	/* 340 */ int32_t t_thicken_x;     /* x-part of string width */
	/* 344 */ int32_t t_thicken_y;     /* y-part of string width */
	/* 348 */ int32_t t_char_x;        /* x-part of string width */
	/* 352 */ int32_t t_char_y;        /* y-part of string width */
	/* 356 */ int32_t t_word_x;        /* x-part of string width */
	/* 360 */ int32_t t_word_y;        /* y-part of string width */
	/* 364 */ int32_t t_string_x;      /* x-part of string width */
	/* 368 */ int32_t t_string_y;      /* y-part of string width */
	/* 372 */ int32_t t_last_x;        /* x-part of width of last character*/
	/* 376 */ int32_t t_last_y;        /* y-part of width of last character*/
	/* 380 */ int16_t t_gtext_spacing; /* 1: use character widths as for v_gtext */
	/* 382 */ int16_t t_xadd;
	/* 384 */ int16_t t_yadd;
	/* 386 */ int16_t t_buf_x1;       /* x1 of bitmap in text buffer */
	/* 388 */ int16_t t_buf_x2;       /* x2 of bitmap in text buffer */
	          int16_t free390;
	          int16_t free392;
	          int16_t free394;
	          int16_t free396;
	          int16_t free398;

/* bitmap description */
	/* 400 */ device_driver_t *device_drvr;  /* pointer to device driver, or NULL */
	/* 404 */ driver_t *bitmap_drvr;  /* pointer to offscreen driver */
	          int16_t free408;
	/* 410 */ organisation_t bitmap_info;
	          int16_t free426;
	          int16_t free428;
	/* 430 */ void *bitmap_addr;    /* pointer to bitmap */
	/* 434 */ int16_t bitmap_width;   /* bytes per line, or 0 for screen drivers */
	/* 436 */ int16_t r_planes;       /* number of planes -1 */
	/* 438 */ int16_t bitmap_off_x;   /* x-offset for coordinates */
	/* 440 */ int16_t bitmap_off_y;   /* y-offset for coordinates */
	/* 442 */ int16_t bitmap_dx;      /* width of bitmap -1 */
	/* 444 */ int16_t bitmap_dy;      /* height of bitmap -1 */
	/* 446 */ int32_t bitmap_len;      /* length of bitmap in bytes */

/* raster operations */
	/* 450 */ void *r_saddr;        /* src address */
	/* 454 */ int16_t r_swidth;       /* bytes per src line */
	/* 456 */ int16_t r_splanes;      /* no of src planes -1 */
	/* 458 */ int32_t r_splane_len;    /* length of a plane */
#define r_snxtword r_splane_len     /* alternative: distance to next word of same plane */
	          char free462[8];
	/* 470 */ void *r_daddr;        /* destination address */
	/* 474 */ int16_t r_dwidth;       /* bytes per destination line */
	/* 476 */ int16_t r_dplanes;      /* no of dst planes -1 */
	/* 478 */ int32_t r_dplane_len;    /* length of a plane */
#define r_dnxtword r_dplane_len     /* alternative: distance to next word of same plane */
	          char free482[8];
	/* 490 */ int16_t r_fgcol;        /* foreground color */
	/* 492 */ int16_t r_bgcol;        /* background color */
	/* 494 */ int16_t r_wmode;        /* operation mode */
	          int32_t free496;
	/* 500 */ void *p_fbox;         /* vector for filled rectangle */
	/* 504 */ void *p_fline;        /* vector for filled line */
	/* 508 */ void *p_hline;        /* vector for horizontal line */
	/* 512 */ void *p_vline;        /* vector for vertical line */
	/* 516 */ void *p_line;         /* vector for diagonal line */
	/* 520 */ void *p_expblt;       /* vector for expanded bitblk transfer */
	/* 524 */ void *p_bitblt;       /* vector for bitblk transfer */
	/* 528 */ void *p_textblt;      /* vector for text blit */
	/* 532 */ void *p_scanline;     /* vector for scanline (seedfill) */
	/* 536 */ void *p_set_pixel;
	/* 540 */ void *p_get_pixel;
	/* 544 */ void *p_transform;
	/* 548 */ void *p_set_pattern;
	/* 552 */ void *p_set_color_rgb;
	/* 556 */ void *p_get_color_rgb;
	/* 560 */ void *p_vdi_to_color;
	/* 564 */ void *p_color_to_vdi;
	          int32_t free568;
	          int32_t free572;
	          int32_t free576;
	/* 580 */ void *p_gtext;
	/* 584 */ void *p_escapes;
	          int32_t free588;
	          int32_t free592;
	/* 596 */ void *wk_owner;       /* pointer to owning application */
	/* 600 WK_LENGTH */
} wk_t;

typedef struct _nvdi_struct_t
{
	/*   0 */ uint16_t version;
	/*   2 */ uint32_t date;
	/*   6 */ uint16_t conf;
	/*   8 */ wk_t *nvdi_wk;
	/*  12 */ int16_t *fills;
	/*  16 */ wk_t **wk_tab;
	/*  20 */ char *gdos_path;
	/*  24 */ void *drivers;
	/*  28 */ void *gdos_fonts;
	/*  32 */ void *fonthdr;
	/*  36 */ void *sys_font_info;
	/*  40 */ uint8_t **colmaptab;
	/*  44 */ int16_t *opnwk_work_out;
	/*  48 */ int16_t *extnd_work_out;
	/*  52 */ int16_t no_wks;
	/*  54 */ int16_t max_vdi;
	/*  56 */ int16_t status;
	/*  58 */ int16_t res58;
	/*  60 */ void *vdi_tab;
	/*  64 */ void *linea_tab;
	/*  68 */ void *gemdos_tab;
	/*  72 */ void *bios_tab;
	/*  76 */ void **xbios_tab;
	/*  80 */ void **mouse_tab;
	/*  84 */ int16_t res84;
	/*  86 */ int16_t blitter;
	/*  88 */ int16_t modecode;
	/*  90 */ int16_t xbios_res;
	/*  92 */ int16_t nvdi_cookie_CPU;
	/*  94 */ int16_t nvdi_cpu_type;
	/*  96 */ int32_t nvdi_cookie_VDO;
	/* 100 */ int32_t nvdi_cookie_MCH;
	/* 104 */ int16_t first_device;
	/* 106 */ int16_t cpu020;
	/* 108 */ int16_t magix;
	/* 110 */ int16_t mint;
	/* 112 */ int32_t (*search_cookie)(int32_t name);
	/* 116 */ int32_t (*init_cookie)(int32_t name, int32_t val);
	/* 120 */ void (*reset_cookie)(int32_t name);
	/* 124 */ void (*init_virtual_vbl)(void);
	/* 128 */ void (*reset_virtual_vbl)(void);
	/* 132 */ void *(*Malloc_sys)(int32_t len);
	/* 136 */ void (*Mfree_sys)(void *addr);
	/* 140 */ void *(*nmalloc)(int32_t len);
	/* 144 */ void (*nmfree)(void *addr);
	/* 148 */ void *(*load_file)(const char *name, int32_t *length);
	/* 152 */ void *(*load_prg)(const char *name);
	/* 156 */ driver_t *(*load_NOD_driver)(organisation_t *info);
	/* 160 */ int16_t (*unload_NOD_driver)(driver_t *drv);
	/* 164 */ int16_t (*init_NOD_drivers)(void);
	/* 168 */ int16_t (*id_to_font_file)(int16_t id, char *file_name);
	/* 172 */ int16_t (*set_FONT_pathes)(int16_t count, int32_t output_vec, char **pathes);
	/* 176 */ int16_t (*get_FONT_path)(int16_t index, char *path);
	/* 180 */ int16_t (*set_caches)(int32_t acache, int32_t bcache, int32_t fcache, int32_t kcache, int32_t wcache, int32_t res);
	/* 184 */ void (*get_caches)(int32_t *acache, int32_t *bcache, int32_t *fcache, int32_t *kcache, int32_t *wcache, int32_t *res);
	/* 188 */ int16_t (*get_FIF_path)(char *path);
	/* 192 */ int16_t (*get_INF_name)(char *name);
	/* 196 */ void *vdi_setup_ptr;
	/* 200 */ void (*vdi_exit)(void);
	/* 204 */ int32_t free204[31];
	/* 296 */
} nvdi_struct_t;

typedef struct _driver_header_t
{
	/*  0 */ uint16_t branch;
	/*  2 */ char magic[8];				/* 'NVDIDRV' or 'OFFSCRN' */
	/* 10 */ int16_t version;			/* driver version; at least 0x290 for *.SYS */
	/* 12 */ uint16_t header_len;		/* length of header */
	/* 14 */ uint16_t type;				/* driver type; 0 for *.OSD, 1 for *.SYS */
	/* 16 */ int32_t (*init)(nvdi_struct_t *nvdi /*, device_driver_t *drv */); /* initialise driver */
	/* 20 */ void (*reset)(nvdi_struct_t *nvdi /*, device_driver_t *drv */); /* release driver */
	/* 24 */ void (*wk_init)(nvdi_struct_t *nvdi); /* initialise workstation */
	/* 28 */ void (*wk_reset)(nvdi_struct_t *nvdi); /* release workstation */
	/* 32 */ void (*opnwkinfo)(int16_t *intout, int16_t *ptsout); /* create output for v_opnwk; change resolution; a6 points to WK */
	/* 36 */ void (*extndinfo)(int16_t *intout, int16_t *ptsout); /* create output for vq_extnd */
	/* 40 */ void (*scrninfo)(int16_t *intout, int16_t *ptsout);  /* create output for vq_scrninfo */
	/* 44 */ char *name;                /* driver name; max 128 bytes incl. zero byte */
	/* 48 */ uint32_t offset_hdr;  /* driver specific */
	/* 52 */ int32_t o52;
	/* 56 */ void *res2[2];             /* reserved */
	/* 64 */ organisation_t info;         /* description of bitmap format */
	/* 80 */ 
} driver_header_t;

#endif
