#include <mint/linea.h>
#include <mint/falcon.h>
#include <mint/sysvars.h>
#include <stdint.h>
#include <stdbool.h>
#include <stddef.h>
#include <string.h>
#include "nvdidefs.h"
#include "screen.h"
#include "glue.h"

const char screen_sys_name[] = SCREEN_SYS;

int32_t	screen_sys_init(vdipb_t* pb, nvdi_struct_t* nvdi, device_driver_t* screen)
{
	static char scrap[256];

	V_HID_CNT = 42;	/* disable cursor (this is how it's done in NVDI.PRG :-) */

	char* dst = scrap;
	const char* src = nvdi->gdos_path;

	while(*src)
		*dst++ = *src++;

	src = screen_init(pb, nvdi);

	if(!src)
	{
		Cconws(screen_sys_name);
		Cconws(": Screen initialisation failed.");
		Cconws("'\r\n");
		Cconin();

		return 0;		
	}

	while(*src)
		*dst++ = *src++;

	*dst++ = '\0';

	screen->addr = nvdi_load_prg(scrap);

	if(!screen->addr)
	{
		screen_exit();

		Cconws(screen_sys_name);
		Cconws(": Failed to load '");
		Cconws(scrap);
		Cconws("'\r\n");
		Cconin();

		return 0;
	}

	screen_sys_header.reset = screen->addr->reset;
	screen->addr->reset = reset;

	screen->wk_len = (int32_t)call_purec_aa(screen->addr->init, nvdi, screen);

	if(!screen->wk_len)
	{
		screen_exit();
		
		Cconws(screen_sys_name);
		Cconws(": Failed to initialise '");
		Cconws(scrap);
		Cconws("'\r\n");
		Cconin();

		return 0;
	}

	screen_post_init(nvdi);

	return screen->wk_len;
}

void screen_sys_reset(nvdi_struct_t* nvdi, device_driver_t* screen)
{
	screen_pre_exit(nvdi);
	screen_exit();

	call_purec_aa(screen_sys_header.reset, nvdi, screen);

	if(screen->addr)
	{
		nvdi_mfree_sys(screen->addr);
		screen->addr = NULL;
	}
}

