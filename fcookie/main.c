#include <stdint.h>
#include <stdlib.h>
#include <stdio.h>
#include <mint/osbind.h>
#include <mint/sysvars.h>

static void uint2con(uint32_t n)
{
    static const char hex[] = "0123456789ABCDEF";

    for(int i = 0; i < 8; i++)
    {
       Cconout(hex[(n >> 28) & 0x0f]);

       n <<= 4;
    }
}

static uint32_t hex2uint(char* str)
{
    uint32_t result = 0;

    while(*str)
    {
        result <<= 4;

        if(*str >= '0' && *str <= '9')
        {
            result += *str - '0';
        }
        else if(*str >= 'A' && *str <= 'F')
        {
            result += *str - 'A' + 10;
        }
        else if(*str >= 'a' && *str <= 'f')
        {
            result += *str - 'a' + 10;
        }

        str++;
    }

    return result;
}

static uint32_t str2uint(char* str)
{
    uint32_t result = 0;

    while(*str)
    {
        result <<= 8;
        result += *str++;
    }

    return result;
}

int main(int argc, char **argv)
{

    if(argc < 2)
    {
        Cconws("Usage: ");
        Cconws(argv[0]);
        Cconws(" cookie [value]");
        Cconws("\r\n");
        Cconws(" - <cookie> supplied in plain text, no quotes (e.g. _VDO).\r\n");
        Cconws(" - <value> supplied in HEX, no prefix (e.g. 0123CAFE).\r\n");
        Cconws("\r\n");
        Cconws("\r\n");

        return 0;
    }

    void* oldssp = (void*)Super(0);

    {
        uint32_t* jar = (uint32_t*)(*_p_cookies);

        if(!jar)
        {
            Cconws("Cookie jar not found.\r\n");
            goto bail; 
        }

        uint32_t cookie = str2uint(argv[1]);

        while(*jar)
        {
            if(jar[0] == cookie)
            {
                if(argc == 3)
                {
                    jar[1] = hex2uint(argv[2]);
                }
                
                Cconws("Cookie ");
                Cconws(argv[1]);
                Cconws(" = ");        
                uint2con(jar[1]);  
                Cconws(".\r\n");   
                Cconws("\r\n");  

                goto done;
            }

            jar++;
            jar++;
        }

        Cconws("Cookie not found.\r\n");
    }

bail:
    SuperToUser(oldssp);
    return 0;

done:
    SuperToUser(oldssp);
    return 0;
}
