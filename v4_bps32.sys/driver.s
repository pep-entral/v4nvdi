         include "const.s"
        
        XREF    _linea_reloc_table
        XREF    scrninfo
        XREF    blink
        XREF    con_state
        XREF    vdi_escape
        XREF    cursconf

; properties for this driver
drv_format					equ format_packed_pixel
drv_planes					equ	32
drv_colours					equ 16777216
drv_clut					equ clut_software
drv_bits_r					equ 8
drv_bits_g					equ 8
drv_bits_b					equ 8
drv_bits_a					equ 0
drv_bits_genlock			equ 0
drv_bits_unused				equ 0
drv_bit_order				equ bit_order_mask_standard

        TEXT 

begin: 
        bra.s       dummy
header:
        dc.b		'NVDIDRV',0		; magic value
        dc.b		5,0,0,80,0,1	; driver version

        dc.l        init 
        dc.l        exit 
        dc.l        primitives 
        dc.l        unknown1 
        dc.l        unknown2 
        dc.l        unknown3 
        dc.l        scrninfo 
        dc.l        name 
        dc.l		0,0,0,0 ; reserved

properties:  
        dc.l		16777216		; number of colours
        dc.w		32              ; number of planes
        dc.w		2               ; pixel format (packed pixel)
        dc.w		1               ; clut type (software)
        dc.w		0,0,0			; reserved

dummy:  rts 

init:
        MOVEM.L   A0-A2/D0-D2,-(A7) 
        BSR       relocate_linea 
        MOVE.L    A0,struct_1
        MOVE.L    A1,struct_2
        MOVE.L    8(A0),sp_struct 
        MOVEA.L   16(A0),A0 
        MOVE.L    -4(A0),struct_3
        MOVEA.L   struct_1,A0
        MOVEA.L   156(A0),A2
        LEA       properties,A0
        JSR       (A2)
        MOVEA.L   struct_2,A1
        MOVE.L    A0,20(A1) 
        BEQ.B     .err 
        BSR       save 
        BSR       install 
        MOVEM.L   A0-A6/D0-D7,-(A7) 
        BSR       init_char 
        MOVE.L    #drv_colours,D0 
        BSR       L0089 
        BSR       init_console 
        BSR       L0069 
        MOVEM.L   (A7)+,A0-A6/D0-D7 
        MOVEM.L   (A7)+,A0-A2/D0-D2 
        MOVE.L    #$B20,D0
        RTS 

.err:   MOVEM.L   (A7)+,A0-A2/D0-D2 
        MOVEQ     #0,D0 
        RTS

save:
        MOVEA.L   struct_1,A0
        MOVEA.L   76(A0),A1 
        MOVE.L    4(A1),xbios_dispatcher_old 

        MOVEA.L   80(A0),A1 
        LEA       L009F,A2
        MOVE.L    (A1)+,(A2)+ 
        MOVE.L    (A1)+,(A2)+ 
        MOVE.L    (A1)+,(A2)+ 

        MOVEA.L   72(A0),A1 
        LEA       L009E,A2
        MOVE.L    (A1)+,(A2)+ 
        MOVE.L    (A1)+,(A2)+ 
        MOVE.L    (A1)+,(A2)+ 
        MOVE.L    (A1)+,(A2)+ 
        MOVE.L    (A1)+,(A2)+ 
        RTS 

install:  
        MOVEA.L   struct_1,A0
        MOVEA.L   76(A0),A1 
        MOVE.L    #xbios_dispatcher,4(A1)

        MOVEA.L   80(A0),A1 
        MOVE.L    #L00A2,(A1) 
        MOVE.L    #sprite_draw,4(A1)
        MOVE.L    #sprite_undraw,8(A1)
        MOVE.W    #1,disab_cnt
        MOVEA.L   72(A0),A1 
        MOVEA.L   8(A1),A2
        MOVE.L    (A2),L00A9
        MOVE.L    #con_out,(A2) 
        MOVE.L    #disab_cnt,(A1) 
        MOVE.L    #blink,4(A1)
        MOVE.L    #con_state,8(A1)
        MOVE.L    #con_out,12(A1) 
        MOVE.L    #ascii_out,16(A1) 
        RTS 

restore:
        MOVEA.L   struct_1,A0
        MOVEA.L   76(A0),A1 
        MOVE.L    xbios_dispatcher_old,4(A1) 
        MOVEA.L   80(A0),A1 
        LEA       L009F,A2
        MOVE.L    (A2)+,(A1)+ 
        MOVE.L    (A2)+,(A1)+ 
        MOVE.L    (A2)+,(A1)+ 
        MOVEA.L   72(A0),A1 
        LEA       L009E,A2
        MOVE.L    (A2)+,(A1)+ 
        MOVE.L    (A2)+,(A1)+ 
        MOVE.L    (A2)+,(A1)+ 
        MOVE.L    (A2)+,(A1)+ 
        MOVE.L    (A2)+,(A1)+ 
        MOVEA.L   72(A0),A1 
        MOVEA.L   8(A1),A2
        MOVE.L    L00A9,(A2)
        RTS

exit:
        MOVEM.L   A0-A2/D0-D2,-(A7) 
        BSR       L008D 
        MOVEM.L   (A7),A0-A2/D0-D2
        MOVEA.L   160(A0),A2
        MOVEA.L   20(A1),A0 
        JSR       (A2)
        BSR       restore 
        MOVEM.L   (A7)+,A0-A2/D0-D2 
        RTS 

unknown1:
        MOVEM.L   A0-A1/D0-D2,-(A7) 
        MOVE.L    632(A6),D0
        BEQ.B     .l1 
        CMP.L     L00A5,D0
        BEQ.B     .l1 
        MOVEA.L   D0,A0 
        BSR       L0096 
.l1:    MOVEM.L   (A7)+,A0-A1/D0-D2 
        RTS 

unknown2:
        MOVEM.L   A0-A2/D0,-(A7)
        MOVEQ     #$2C,D0 
        LEA       _DEV_TAB.W,A2
.l1:    MOVE.W    (A2)+,(A0)+ 
        DBF       D0,.l1
        MOVEQ     #$B,D0
        LEA       _SIZ_TAB.W,A2
.l2:    MOVE.W    (A2)+,(A1)+ 
        DBF       D0,.l2
        MOVEM.L   (A7)+,A0-A2/D0
        RTS 

unknown3:
        MOVEM.L   A0-A2/D0,-(A7)
        LEA       _INQ_TAB.W,A2
        MOVEQ     #$2C,D0 
.l1:    MOVE.W    (A2)+,(A0)+ 
        DBF       D0,.l1
        MOVE.W    #1,-52(A0)
        MOVEA.L   struct_1,A2
        BTST      #1,7(A2)
        BEQ.B     .l2 
        CLR.W     -80(A0) 
.l2:    MOVEA.L   48(A2),A2 
        LEA       90(A2),A2 
        MOVEQ     #$B,D0
.l3:    MOVE.W    (A2)+,(A1)+ 
        DBF       D0,.l3
        LEA       52(A6),A2 
        MOVE.L    (A2)+,-24(A1) 
        MOVE.L    (A2)+,-20(A1) 
        MOVEM.L   (A7)+,A0-A2/D0
        RTS 

relocate_linea:
        MOVEM.L   A0-A2/D0-D2,-(A7) 
        dc.w      $A000 
        SUB.W     #_v_planes,D0 
        BEQ.B     .l2 
        LEA       begin,A0
        LEA       _linea_reloc_table,A1
.l1:    MOVE.W    (A1)+,D1
        BEQ.B     .l2 
        ADDA.W    D1,A0 
        ADD.W     D0,(A0) 
        BRA.B     .l1 
.l2:    MOVEM.L   (A7)+,A0-A2/D0-D2 
        RTS 

primitives:
        MOVE.L    _DEV_TAB.W,16(A6)
        MOVE.L    (_DEV_TAB+6).W,12(A6)
        MOVE.W    #$1F,436(A6) 
        MOVE.W    #$FF,20(A6) 
        MOVE.L    16(A6),56(A6) 
        LEA       properties,A0
        MOVE.L    (A0)+,410(A6) 
        MOVE.W    (A0)+,414(A6) 
        MOVE.W    (A0)+,416(A6) 
        MOVE.W    (A0)+,418(A6) 
        MOVE.L    584(A6),vdi_escape_old 
        MOVE.L    #vdi_escape,584(A6)
        MOVE.L    L00A6,636(A6) 
        MOVE.L    #$1000000,D0 
        BSR       L0095 
        MOVE.L    A0,632(A6)
        BNE.B     .l1 
        MOVE.L    L00A5,632(A6) 
.l1:    MOVEQ     #1,D0 
        RTS 

nothing:
        RTS 

L0069:  MOVEM.L   A0-A2/D0-D2,-(A7)     ; called during init
        MOVE.W    #$20,_v_planes.W      ; FIXME! Det med .equ
        LEA       _DEV_TAB.W,A1
        MOVE.W    v_hz_rez.W,(A1)
        SUBQ.W    #1,(A1)+
        MOVE.W    v_vt_rez.W,(A1)
        SUBQ.W    #1,(A1)+
        CLR.W     (A1)+ 
        MOVE.L    (A2)+,(A1)+ 
        MOVEQ     #$27,D0 
        MOVEA.L   struct_1,A0
        MOVEA.L   44(A0),A0 
        LEA       10(A0),A0 
.l1:    MOVE.W    (A0)+,(A1)+ 
        DBF       D0,.l1
        MOVEA.L   struct_1,A1
        MOVE.W    88(A1),D0 
        MOVE.W    #$109,D1
        CMPI.W    #$280,v_hz_rez.W 
        BGE.B     .l2 
        ADD.W     D1,D1 
.l2:    MOVE.W    D1,(_DEV_TAB+6).W
        MOVE.W    #$109,D1
        CMPI.W    #$190,v_vt_rez.W 
        BGE.B     .l3 
        ADD.W     D1,D1 
.l3:    MOVE.W    D1,(_DEV_TAB+8).w       
        MOVE.W    #$100,(_DEV_TAB+26).w  
        MOVE.W    #1,(_DEV_TAB+70).w   
        CLR.W     (_DEV_TAB+78).w   
        MOVEQ     #$B,D0
        LEA       _SIZ_TAB.W,A1
.l4:    MOVE.W    (A0)+,(A1)+ 
        DBF       D0,.l4
        MOVEQ     #$2C,D0 
        MOVEA.L   struct_1,A0
        MOVEA.L   48(A0),A0 
        LEA       _INQ_TAB.W,A1
.l5:    MOVE.W    (A0)+,(A1)+ 
        DBF       D0,.l5
        MOVE.W    (_DEV_TAB+78).W,(_INQ_TAB+2).W       
        MOVE.W    _v_planes.W,(_INQ_TAB+8).W      
        MOVE.W    (_DEV_TAB+70).W,(_INQ_TAB+10).W  
        MOVE.W    #2200,(_INQ_TAB+12).W
        MOVEM.L   (A7)+,A0-A2/D0-D2 
        RTS 


xbios_dispatcher:
        CMP.W     #$15,D0 
        BEQ.B     xbios_cursconf 

        CMP.W     #$40,D0 
        BEQ.B     xbios_blitmode 

        CMP.W     #$96,D0 
        BEQ.B     xbios_vsetmask 

        MOVEA.L   xbios_dispatcher_old,A1
        JMP       (A1)

xbios_cursconf:
        jmp       cursconf

xbios_blitmode:
        MOVEQ     #0,D0 
        RTE 

xbios_vsetmask:
        MOVE.W    (A0)+,L00A3 
        MOVE.W    (A0)+,L00A4 
        RTE 

L0089:  MOVE.L    D3,-(A7)      ; called from init
        MOVE.L    D0,D3 
        JSR       L0090 
        TST.L     D0
        BEQ.B     .l2 
        TST.L     D3
        BEQ.B     .l1 
        CMP.L     #$100,D3
        BGT.B     .l1 
        MOVEA.L   L00A5,A0
        LEA       48(A0),A0 
        MOVEQ     #-1,D1 
        ADD.L     D3,D1 
        MOVEQ     #0,D0 
        JSR       nothing 
.l1:    MOVEQ     #1,D0 
        BRA.B     .l3 

.l2:    MOVEQ     #0,D0 
.l3:    MOVE.L    (A7)+,D3
        RTS 

L008D:  MOVE.L    L00A5,D0      ; called from exit
        BEQ.B     .l1 
        MOVEA.L   D0,A0 
        MOVEA.L   struct_1,A1
        MOVEA.L   136(A1),A1
        JSR       (A1)
        CLR.L     L00A5 
.l1:    MOVE.L    L00A6,D0
        BEQ.B     .l2 
        MOVEA.L   D0,A0 
        MOVEA.L   struct_1,A1
        MOVEA.L   136(A1),A1
        JSR       (A1)
        CLR.L     L00A6 
.l2:    RTS 

L0090:  MOVE.L    D3,-(A7)      ; called from L0089
        MOVE.L    D4,-(A7)
        MOVE.L    D0,D3 
        MOVEQ     #1,D1 
        MOVEA.L   struct_1,A0
        MOVEA.L   300(A0),A0
        JSR       (A0)
        MOVE.L    A0,L00A5
        MOVE.L    A0,D0 
        BEQ.B     .l3 
        MOVEQ     #$10,D1 
        CMP.L     D3,D1 
        BLT.B     .l1 
        MOVEQ     #3,D4 
        BRA.B     .l2 

.l1:    MOVEQ     #4,D4 
.l2:    MOVEQ     #1,D1 
        LSL.W     D4,D1 
        MOVE.W    D4,D0 
        MOVEA.L   L00A5,A0
        MOVEA.L   struct_1,A1
        MOVEA.L   304(A1),A1
        JSR       (A1)
        MOVE.L    A0,L00A6
        MOVEQ     #1,D0 
        BRA.B     .l4 

.l3:    CLR.L     L00A6 
        MOVEQ     #0,D0 
.l4:    MOVE.L    (A7)+,D4
        MOVE.L    (A7)+,D3
        RTS 

L0095:  MOVEQ     #1,D1             ; called by primitives
        MOVEA.L   struct_1,A0
        MOVEA.L   300(A0),A0
        JSR       (A0)
        RTS 

L0096:  MOVEA.L   struct_1,A1          ; called by unknown
        MOVEA.L   136(A1),A1
        JSR       (A1)
        RTS 

        EVEN

        DATA 

name:
        dc.b	'Vampire V4 16M colours',0 
    
        BSS
      
        XDEF    sp_struct
        XDEF    struct_1
        XDEF    struct_2
        XDEF    struct_3
        XDEF    vdi_escape_old
            
regs_save:         ds.l      16
struct_1:          DS.L      1 
struct_2:          DS.L      1 
struct_3:          DS.L      1 
sp_struct:
        ds.l    1

xbios_dispatcher_old:
                DS.L      1 
L009E:          DS.B      200
L009F:          DS.B      160
vdi_escape_old:    
                DS.L      1 
blit_old:       DS.L      1 
rect_old:       DS.L      1
L00A2:          DS.B      5200
L00A3:          DS.W      1 
L00A4:          DS.W      1 
L00A5:          DS.L      1 
L00A6:          DS.L      1 
L00A9:          DS.L      1 

        END

