#include <stdint.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#undef GCC

//typedef struct
//{
//	int16_t  ph_branch;			/* Branch to start of the program  */
//								/* (must be 0x601a!)               */
//	int32_t  ph_tlen;			/* Length of the TEXT segment      */
//	int32_t  ph_dlen;			/* Length of the DATA segment      */
//	int32_t  ph_blen;			/* Length of the BSS segment       */
//	int32_t  ph_slen;			/* Length of the symbol table      */
//	int32_t  ph_res1;			/* Reserved, should be 0;          */
//								/* Required by PureC               */
//	int32_t  ph_prgflags;		/* Program flags                   */
//	int16_t  ph_absflag;		/* 0 = Relocation info present     */
//} PH;

int32_t fsize ( FILE * hFile )
{
	int32_t lCurPos, lEndPos;

	if (!hFile)
		return 0;

	lCurPos = ftell ( hFile );
	fseek ( hFile, 0, 2 );
	lEndPos = ftell ( hFile );
	fseek ( hFile, lCurPos, 0 );
	return lEndPos;
}

int main(int argc, char **argv)
{	
	int32_t i,j,k;
	
	if(argc == 4)
	{
		uint8_t		*ph1,*ph2;
		FILE		*fd1, *fd2;
		int32_t		size1,size2;
		uint8_t		*ptr1, *ptr2;
		
		/*
		 * Load binaries into memory!
		 *  - we can't use Pexec(), since we don't want relocation to change
		 *    the binary contents.
		 */
		
		if(!(fd1 = fopen(argv[1], "rb")))
		{
			printf("Couldn't open file (%s)\r\n", argv[1]);
			return -1;
		}
		size1 = fsize(fd1);
				
		if(!(fd2 = fopen(argv[2], "rb")))
		{
			printf("Couldn't open file (%s)\r\n", argv[2]);
			return -1;
		}
		size2 = fsize(fd2);
		
		/* Check file sizes */
		if(size1 != size2)
		{
			printf("File size mismatch.\r\n");
			return -1;
		}
		
		if(!size1)
		{
			printf("Null size.\r\n");
			return -1;
		}
				
		/* Load binaries into memory */
		if(!(ph1 = (uint8_t*)malloc(size1)))
		{
			printf("Couldn't allocate memory (%s)\r\n", argv[1]);
			return -1;
		}
		if(!(ph2 = (uint8_t*)malloc(size2)))
		{
			printf("Couldn't allocate memory (%s)\r\n", argv[2]);
			return -1;
		}

		if(fread(ph1, 1, size1, fd1) < size1)
		{
			printf("File load error (%s)\r\n", argv[1]);
			return -1;
		}

		if(fread(ph2, 1, size2, fd2) < size2)
		{
			printf("File load error (%s)\r\n", argv[2]);
			return -1;
		}
	
		fclose(fd1);
		fclose(fd2);
		
		/* Make sure binaries are valid */

		static const uint8_t magic[2] = {0x60, 0x1a};

		if(memcmp(magic, ph1, sizeof(magic)))
		{
			printf("Not a valid binary (%s)r\n", argv[1]);
			return -1;
		}
		if(memcmp(magic, ph2, sizeof(magic)))
		{
			printf("Not a valid binary (%s)r\n", argv[2]);
			return -1;
		}
		
		uint32_t tlen1 = (ph1[ 2] << 24) | (ph1[ 3] << 16) | (ph1[ 4] << 8) | ph1[ 5];
		uint32_t dlen1 = (ph1[ 6] << 24) | (ph1[ 7] << 16) | (ph1[ 8] << 8) | ph1[ 9];
		uint32_t blen1 = (ph1[10] << 24) | (ph1[11] << 16) | (ph1[12] << 8) | ph1[13];
		uint32_t slen1 = (ph1[14] << 24) | (ph1[15] << 16) | (ph1[16] << 8) | ph1[17];
		uint32_t tlen2 = (ph2[ 2] << 24) | (ph2[ 3] << 16) | (ph2[ 4] << 8) | ph2[ 5];
		uint32_t dlen2 = (ph2[ 6] << 24) | (ph2[ 7] << 16) | (ph2[ 8] << 8) | ph2[ 9];
		uint32_t blen2 = (ph2[10] << 24) | (ph2[11] << 16) | (ph2[12] << 8) | ph2[13];
		uint32_t slen2 = (ph2[14] << 24) | (ph2[15] << 16) | (ph2[16] << 8) | ph2[17];

		/* Compare binary properties */
		if(tlen1 != tlen2)
		{
			printf("Text segment mismatch\r\n");
			return -1;
		}
		if(dlen1 != dlen2)
		{
			printf("Data segment mismatch\r\n");
			return -1;
		}
		if(blen1 != blen2)
		{
			printf("BSS segment mismatch\r\n");
			return -1;
		}
		if(slen1 != slen2)
		{
			printf("Symbol segment mismatch\r\n");
			return -1;
		}

		/* Create diff */
		if(!(fd1 = fopen(argv[3], "wb")))
		{
			printf("Couldn't create target file (%s)\r\n", argv[3]);
			return -1;
		}
		
		/* Perform diff */
		printf("Creating relocation table... ");
		ptr1 = &ph1[28];
		ptr2 = &ph2[28];
#ifdef GCC
		fprintf(fd1,"    .globl _linea_reloc_table\r\n");
		fprintf(fd1,"    .data\r\n");
#else
		fprintf(fd1,"    xdef _linea_reloc_table\r\n");
		fprintf(fd1,"    DATA\r\n");
#endif
		fprintf(fd1,"_linea_reloc_table:\r\n");

		printf("text len: %d\r\n", tlen1);

		for(i = j = k = 0; i < tlen1; i++)
		{			
			if(*ptr1 != *ptr2)
			{
#ifdef GCC
				fprintf(fd1, "%s 0x%04x",k ? "," : "\r\n    dc.w", j);
#else
				fprintf(fd1, "%s $%04x",k ? "," : "\r\n    dc.w", j);
#endif
                		printf("relocating: 0x%04x at offset %d\r\n", (ptr1[0] << 8) | ptr1[1], j);
			
				j = 1;
				
				ptr1++;
				ptr2++;
				i++;
				
				k++;
				k&=7;
			}
			
			j++;
			
			ptr1++;
			ptr2++;
		}
		fprintf(fd1,"\r\n    dc.w 0\r\n\r\n");
#ifdef GCC
        fprintf(fd1,"\r\n\r\n    .end\r\n");
#endif
		fclose(fd1);
		printf("Done.\r\n");
		return 0;
	}
	printf("Bad input.\r\n");	
}