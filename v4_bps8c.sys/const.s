; system variables
v_bas_ad					equ $44E	
conterm						equ $484	
bell_hook					equ $5AC	

; linea variables
_bytes_lin					equ ($206E+larel)
v_vt_rez					equ ($206C+larel)
v_stat_0					equ ($206A+larel)
v_hz_rez					equ ($2064+larel)
v_fnt_wr					equ ($2062+larel)
v_cur_cx					equ ($2054+larel)
v_cur_cy					equ ($2056+larel)
vct_init					equ ($2058+larel)
v_cur_tim					equ ($2059+larel)
v_fnt_ad					equ ($205A+larel)
v_cur_ad					equ ($204E+larel)
v_cel_mx					equ ($2044+larel)
v_cel_my					equ ($2046+larel)
v_cel_wr					equ ($2048+larel)
v_cel_ht					equ ($2042+larel)
v_col_bg					equ ($204A+larel)
v_col_fg					equ ($204C+larel)
_v_planes					equ ($2070+larel)
sav_cx						equ ($1F22+larel)
_INQ_TAB					equ ($1D62+larel)
_SIZ_TAB					equ ($1E7E+larel)
_DEV_TAB					equ ($1DBC+larel)


; constants
format_interleaved_planes	equ 0		
format_consecutive_planes	equ 1
format_packed_pixel			equ 2

clut_none					equ 0
clut_hardware				equ 1
clut_software				equ 2

bit_order_mask_standard		equ 1
bit_order_mask_falcon		equ 2		; hicolor/tc modes only
bit_order_mask_swapped		equ 128		; hicolor/tc modes only