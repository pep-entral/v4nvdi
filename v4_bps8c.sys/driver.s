; L0041 avg�r p� n�t vis vilket palett-djup jag har, m�jligen n�t machine-ID

	xref _linea_reloc_table

; system variables
v_bas_ad					equ $44E	
conterm						equ $484	
bell_hook					equ $5AC	

; linea variables
_bytes_lin					equ ($206E+larel)
v_vt_rez					equ ($206C+larel)
v_stat_0					equ ($206A+larel)
v_hz_rez					equ ($2064+larel)
v_fnt_wr					equ ($2062+larel)
v_cur_cx					equ ($2054+larel)
v_cur_cy					equ ($2056+larel)
vct_init					equ ($2058+larel)
v_cur_tim					equ ($2059+larel)
v_fnt_ad					equ ($205A+larel)
v_cur_ad					equ ($204E+larel)
v_cel_mx					equ ($2044+larel)
v_cel_my					equ ($2046+larel)
v_cel_wr					equ ($2048+larel)
v_cel_ht					equ ($2042+larel)
v_col_bg					equ ($204A+larel)
v_col_fg					equ ($204C+larel)
_v_planes					equ ($2070+larel)
sav_cx						equ ($1F22+larel)
_INQ_TAB					equ ($1D62+larel)
_SIZ_TAB					equ ($1E7E+larel)
_DEV_TAB					equ ($1DBC+larel)

; constants
format_interleaved_planes	equ 0		
format_consecutive_planes	equ 1
format_packed_pixel			equ 2

clut_none					equ 0
clut_hardware				equ 1
clut_software				equ 2

bit_order_mask_standard		equ 1
bit_order_mask_falcon		equ 2		; hicolor/tc modes only
bit_order_mask_swapped		equ 128		; hicolor/tc modes only

; properties for this driver
drv_format					equ format_packed_pixel
drv_planes					equ	8
drv_colours					equ 256
drv_clut					equ clut_hardware
drv_bits_r					equ 8
drv_bits_g					equ 8
drv_bits_b					equ 8
drv_bits_a					equ 0
drv_bits_genlock			equ 0
drv_bits_unused				equ 0
drv_bit_order				equ bit_order_mask_standard

		TEXT 

begin:
		bra.s     dummy 

header:
		dc.b		'NVDIDRV',0		; magic value
		dc.b		5,0,0,80,0,1	; driver version
		
		dc.l		init 
		dc.l		exit 
		dc.l		primitives 
		dc.l		unknown1 
		dc.l		unknown2 
		dc.l		unknown3 
		dc.l		scrninfo 
		dc.l		name 
		dc.l		0,0,0,0 ; reserved

properties:
		dc.l		drv_colours		; number of colours
		dc.w		drv_planes		; number of planes
		dc.w		drv_format		; pixel format (packed pixel)
		dc.w		drv_clut		; clut type (hardware)
		dc.w		  0,0,0			; reserved
dummy:	rts 

init:	movem.l   A0-A2/D0-D2,-(A7) 
		bsr		 relocate_linea 
		move.l    A0,X0043
		move.l    A1,X0044
		move.l    8(A0),L0090 
		movea.l   16(A0),A0 
		move.l    -4(A0),L0091
		movea.l   X0043,A0
		movea.l   156(A0),A2
		lea		 properties,A0
		jsr		 (A2)
		movea.l   X0044,A1
		move.l    A0,20(A1) 
		beq.s     .err 
		bsr		 save 
		bsr		 install 
		bsr		init_char
		movea.l   X0043,A0
		move.w    106(A0),X0040 
		move.w    96(A0),X0041
		move.w    88(A0),X0042
		bsr		 L0015 
		move.l    #drv_colours,D0
		bsr		 L0032x 
		bsr		init_console
		movem.l   (A7)+,A0-A2/D0-D2 
		move.l    #$520,D0
		rts 
		
.err:	movem.l   (A7)+,A0-A2/D0-D2 
		moveq     #0,D0 
		rts
		 
save:	movea.l   X0043,A0
		movea.l   76(A0),A1 
		move.l    4(A1),xbios_dispatcher_old 
		
		movea.l   80(A0),A1 
		lea		 L012B,A2
		move.l    (A1)+,(A2)+ 
		move.l    (A1)+,(A2)+ 
		move.l    (A1)+,(A2)+ 
		
		movea.l   72(A0),A1 
		lea		 L012A,A2
		move.l    (A1)+,(A2)+ 
		move.l    (A1)+,(A2)+ 
		move.l    (A1)+,(A2)+ 
		move.l    (A1)+,(A2)+ 
		move.l    (A1)+,(A2)+ 

		rts
		 
install:
		movea.l   X0043,A0
		movea.l   76(A0),A1 
		move.l    #xbios_dispatcher,4(A1)
	
		movea.l   80(A0),A1
		move.l    #L012C,(A1)
		move.l    #sprite_draw,4(A1)
		move.l    #sprite_undraw,8(A1)
		move.w    #1,disab_cnt
		movea.l   72(A0),A1 
		movea.l   8(A1),A2
		move.l    (A2),L0131
		move.l    #con_out,(A2) 
		move.l    #disab_cnt,(A1) 
		move.l    #blink,4(A1)
		move.l    #con_state,8(A1)
		move.l    #con_out,12(A1) 
		move.l    #ascii_out,16(A1) 
		rts
		
restore:
		movea.l   X0043,A0
		movea.l   76(A0),A1 
		move.l    xbios_dispatcher_old,4(A1) 
		
		movea.l   80(A0),A1 
		lea		 L012B,A2
		move.l    (A2)+,(A1)+ 
		move.l    (A2)+,(A1)+ 
		move.l    (A2)+,(A1)+ 
		
		movea.l   72(A0),A1 
		lea		 L012A,A2
		move.l    (A2)+,(A1)+ 
		move.l    (A2)+,(A1)+ 
		move.l    (A2)+,(A1)+ 
		move.l    (A2)+,(A1)+ 
		move.l    (A2)+,(A1)+ 
		movea.l   72(A0),A1 
		movea.l   8(A1),A2
		move.l    L0131,(A2)

		rts
		 
exit:	movem.l   A0-A2/D0-D2,-(A7) 
		bsr		 L0036x 
		movem.l   (A7),A0-A2/D0-D2
		movea.l   160(A0),A2
		movea.l   20(A1),A0 
		jsr		 (A2)
		bsr.s     restore 
		movem.l   (A7)+,A0-A2/D0-D2 
		rts 
		
unknown2:
		movem.l   A0-A2/D0,-(A7)
		; A0 = dest (from caller)
		moveq     #44,D0 
		lea		 _DEV_TAB.w,A2		; source = DEV_TAB
.l1:	move.w    (A2)+,(A0)+ 
		dbf		 D0,.l1
		
		; A1 = dest (from caller)
		moveq     #11,D0
		lea		 _SIZ_TAB.w,A2		; source = SIZ_TAB
.l2:	move.w    (A2)+,(A1)+ 
		dbf		 D0,.l2
		
		movem.l   (A7)+,A0-A2/D0
		rts 
		
unknown3:
		movem.l   A0-A2/D0,-(A7)
		; A0 = dest (from caller)
		lea		 _INQ_TAB.w,A2		; source = INQ_TAB
		moveq     #44,D0 
.l1:	move.w    (A2)+,(A0)+ 
		dbf		 D0,.l1
		
		move.w    #1,-52(A0)
		movea.l   X0043,A2
		movea.l   48(A2),A2 
		lea		 90(A2),A2			; src = some internal NVDI thing
		
		; A1 = dest (from caller)
		moveq     #11,D0
.l2:	move.w    (A2)+,(A1)+ 
		dbf		 D0,.l2
		
		lea		 52(A6),A2 
		move.l    (A2)+,-24(A1) 
		move.l    (A2)+,-20(A1) 
		movem.l   (A7)+,A0-A2/D0
		rts 
		
; set up structure returned by vq_scrninfo()
scrninfo:
		movem.l   A0-A1/D0-D2,-(A7) 
		move.w    #drv_format,(A0)+		; format = packed pixels
		move.w    #drv_clut,(A0)+			; hardware clut
		move.w    #drv_planes,(A0)+		; number of planes
		move.l    #drv_colours,(A0)+		; number of colours
		move.w    _bytes_lin.w,(A0)+		; line width in bytes
		move.l    v_bas_ad.w,(A0)+		; bitmap addr
		move.w    #drv_bits_r,(A0)+		; bits for Red
		move.w    #drv_bits_g,(A0)+		; bits for Green
		move.w    #drv_bits_b,(A0)+		; bits for Blue
		move.w    #drv_bits_a,(A0)+		; bits for Alpha
		move.w    #drv_bits_genlock,(A0)+	; bits for Genlock
		move.w    #drv_bits_unused,(A0)+	; unused bits
		move.w    #drv_bit_order,(A0)+	; bit order = standard
		clr.w     (A0)+					; reserved
		
		move.w    #$FF,D0					; setup pixel value vs. VDI-index
		moveq     #0,D1 
		movea.l   X0043,A1
		movea.l   40(A1),A1				; information availble inside NVDI already
		movea.l   (A1),A1 
.l1:	move.b    (A1)+,D1
		move.w    D1,(A0)+				; copy it to structure
		dbf		 D0,.l1
		movem.l   (A7)+,A0-A1/D0-D2 
		rts 
		
; traverse the relocation table and change all entries according
; to the current LineA-base
relocate_linea:
		movem.l   A0-A2/D0-D2,-(A7) 
		dc.w		$A000 
		sub.w     #_v_planes,D0 
		beq.s     .l2 
		lea		 begin,A0
		lea		 _linea_reloc_table,A1
.l1:	move.w    (A1)+,D1
		beq.s     .l2 
		adda.w    D1,A0 
		add.w     D0,(A0) 
		bra.s     .l1
		
.l2:	movem.l   (A7)+,A0-A2/D0-D2 
		rts 
		
L0015:	movem.l   A0-A2/D0-D2,-(A7) 
 
		bsr		 .l8 

.l2:	lea		 _DEV_TAB.w,A1		; destination = DEV_TAB
		move.w    v_hz_rez.w,(A1)
		subq.w    #1,(A1)+
		move.w    v_vt_rez.w,(A1)
		subq.w    #1,(A1)+
		clr.w     (A1)+ 
		move.l    (A2)+,(A1)+ 
		
		moveq     #39,D0 
		movea.l   X0043,A0
		movea.l   44(A0),A0 
		lea		 10(A0),A0 
.l3:	move.w    (A0)+,(A1)+ 
		dbf		 D0,.l3
		
		move.w    (A2)+,(_DEV_TAB+26).w ; Number of simultaneously depicted colours (e.g. 256 on an 8 bit screen)
		move.w    (A2)+,(_DEV_TAB+70).w ; Colour capability flag
		move.w    (A2)+,(_DEV_TAB+78).w ; Number of colour levels available, or: 0 = More than 32767, 2 = Monochrome
		
		moveq     #11,D0
		lea		 _SIZ_TAB.w,A1
.l4:	move.w    (A0)+,(A1)+ 
		dbf		 D0,.l4
		
		moveq     #44,D0 
		movea.l   X0043,A0
		movea.l   48(A0),A0 
		lea		 _INQ_TAB.w,A1
.l5:	move.w    (A0)+,(A1)+ 
		dbf		 D0,.l5
		
		move.w    (_DEV_TAB+78).w,(_INQ_TAB+2).w	; Number of colour levels (refers to the CLUT)
		move.w    _v_planes.w,(_INQ_TAB+8).w		; Number of planes
		move.w    (_DEV_TAB+70).w,(_INQ_TAB+10).w	; CLUT flag (0 = no CLUT, 1 = CLUT or pseudo-CLUT (TrueColor) exists)
		bne.s     .l6 
		move.w    #1,(_INQ_TAB+2).w		; Number of colour levels (refers to the CLUT)
.l6:	move.w    #5000,(_INQ_TAB+12).w	; Performance factor (number of 16*16 raster operations per second)
.l7:	movem.l   (A7)+,A0-A2/D0-D2 
		rts 
		
.l8:	movem.l   A0-A1/D0-D2,-(A7) 
		move.w    X0042,D0
		lea		 X0046,A0
		movea.l   A0,A2 
		move.w    #265,D1
		cmpi.w    #640,v_hz_rez.w 
		bge.s     .l9 
		add.w     D1,D1 
.l9:	move.w    D1,(A0)+
		move.w    #265,D1
		cmpi.w    #400,v_vt_rez.w 
		bge.s     .l10 
		add.w     D1,D1 
.l10:	move.w    D1,(A0)+
		moveq     #7,D1 
		and.w     D0,D1 
		add.w     D1,D1 
		move.w    #$100,(A0)+ 
		move.w    #1,(A0)+
		move.w    #$1000,(A0)+
		btst		#7,D0 
		bne.s     .l11 
		clr.w     -2(A0)
.l11:	movem.l   (A7)+,A0-A1/D0-D2 
		rts 
							
primitives:
		move.l    _DEV_TAB.w,16(A6)		; Maximum addressable width/height
		move.l    (_DEV_TAB+6).w,12(A6)	; Pixel width/height in microns
		move.w    #7,436(A6)
		move.w    #$FF,20(A6) 
		move.l    16(A6),56(A6) 
		lea		  properties,A0
		move.l    (A0)+,410(A6) 
		move.w    (A0)+,414(A6) 
		move.w    (A0)+,416(A6) 
		move.w    (A0)+,418(A6) 
		move.l    X0047,632(A6) 
		move.l    X0048,636(A6) 
		move.l    584(A6),vdi_escape_old ; console
		move.l    #vdi_escape,584(A6) ; replace console
 		move.l    #set_pal,552(A6)
		move.l    #get_pal,556(A6)
		moveq     #1,D0 
		rts 
		
unknown1:
		rts
 
set_pal:
		movem.l   A2/D3-D5,-(A7)
		move.l    D0,D4 
		move.l    D1,D5 
		movea.l   A0,A2 
		
.lp1:	move.l    (A2)+,D0
		move.w    (A2)+,D1
		move.w    (A2)+,D2
		move.l    D4,D3 
		move.l    A2,-(A7)
		clr.l     -(A7) 
		movea.l   A7,A0 
		addq.l    #1,A0 
		move.b    D0,(A0)+
		move.b    D1,(A0)+
		move.b    D2,(A0)+
		move.l    A7,-(A7)
		move.w    #1,-(A7)
		move.w    D3,-(A7)
		move.w    #$5D,-(A7)	; VsetRGB()
		trap	  #$E 
		lea		  14(A7),A7 
		movea.l   (A7)+,A2
		addq.l    #1,D4 
		cmp.l     D4,D5 
		bge.s     .lp1 
		movem.l   (A7)+,A2/D3-D5
		rts 
		
get_pal:
		movem.l   A2/D3-D5,-(A7)
		move.l    D0,D4 
		move.l    D1,D5 
		movea.l   A0,A2
		 
.lp1:	move.l    D4,D0 
		move.l    A2,-(A7)
		clr.l     -(A7) 
		move.l    A7,-(A7)
		move.w    #1,-(A7)
		move.w    D0,-(A7)
		move.w    #$5E,-(A7) ; VgetRGB()
		trap	  #$E 
		lea		  10(A7),A7 
		movea.l   A7,A0 
		addq.l    #1,A0 
		move.b    (A0),D0 
		lsl.w     #8,D0 
		move.b    (A0)+,D0
		move.b    (A0),D1 
		lsl.w     #8,D1 
		move.b    (A0)+,D1
		move.b    (A0),D2 
		lsl.w     #8,D2 
		move.b    (A0)+,D2
		addq.l    #4,A7 
		movea.l   (A7)+,A2		
		move.w    D4,(A2)+
		move.w    D0,(A2)+
		move.w    D1,(A2)+
		move.w    D2,(A2)+
		addq.l    #1,D4 
		cmp.l     D4,D5 
		bge.s     .lp1 
		
		movem.l   (A7)+,A2/D3-D5
		rts 
		
		
L0032x: ; called during init
		move.l    D3,-(A7)
		move.l    D0,D3 
		jsr		 L0039x
		tst.l     D0
		beq.s     .l2 
		tst.l     D3
		beq.s     .l1 
		cmp.l     #$100,D3
		bgt.s     .l1 
		movea.l   X0047,A0
		lea		 48(A0),A0 
		moveq     #-1,D1
		add.l     D3,D1 
		moveq     #0,D0 
		jsr		 set_pal 
.l1:	moveq     #1,D0 
		bra.s     .l3 
		
.l2:	moveq     #0,D0 
.l3:	move.l    (A7)+,D3
		rts 
		
L0036x: ; called during exit
		move.l    X0047,D0
		beq.s     .l1 
		movea.l   D0,A0 
		movea.l   X0043,A1
		movea.l   136(A1),A1
		jsr		 (A1)
		clr.l     X0047 
.l1:	move.l    X0048,D0
		beq.s     .l2 
		movea.l   D0,A0 
		movea.l   X0043,A1
		movea.l   136(A1),A1
		jsr		 (A1)
		clr.l     X0048 
.l2:	rts 

L0039x:
		move.l    D3,-(A7)
		move.l    D4,-(A7)
		move.l    D0,D3 
		moveq     #1,D1 
		movea.l   X0043,A0
		movea.l   300(A0),A0
		jsr		 (A0)
		move.l    A0,X0047
		move.l    A0,D0 
		beq.s     .l3 
		moveq     #$10,D1 
		cmp.l     D3,D1 
		blt.s     .l1 
		moveq     #3,D4 
		bra.s     .l2 
		
.l1:	moveq     #4,D4 
.l2:	moveq     #1,D1 
		lsl.w     D4,D1 
		move.w    D4,D0 
		movea.l   X0047,A0
		movea.l   X0043,A1
		movea.l   304(A1),A1
		jsr		 (A1)
		move.l    A0,X0048
		moveq     #1,D0 
		bra.s     .l4 
		
.l3:	clr.l     X0048
		moveq     #0,D0 
.l4:	move.l    (A7)+,D4
		move.l    (A7)+,D3
		rts

sprite_undraw:
		move.w    (A2)+,D2
		subq.w    #1,D2 
		bmi.s     .done 
		movea.l   (A2)+,A1
		BCLR		#0,(A2) 
		beq.s     .done 
		movea.w   _bytes_lin.w,A3
		lea		 -20(A3),A3
		addq.l    #2,A2 
.lp1:	move.l    (A2)+,(A1)+ 
		move.l    (A2)+,(A1)+ 
		move.l    (A2)+,(A1)+ 
		move.l    (A2)+,(A1)+ 
		move.l    (A2)+,(A1)+ 
		adda.w    A3,A1 
		dbf		 D2,.lp1
		
.done:	rts 


sprite_draw:
		moveq     #$F,D2
		moveq     #$F,D3
		move.b    7(A0),D6
		move.b    9(A0),D7
		sub.w     (A0)+,D0
		bpl.s     .lp1 
		add.w     D0,D2 
		bmi		 .done 
.lp1:	move.w    _DEV_TAB.w,D4			; Maximum addressable width (0 - Xmax)
		subi.w    #$F,D4
		sub.w     D0,D4 
		bge.s     .lp2 
		add.w     D4,D2 
		bmi		 .done 
.lp2:	sub.w     (A0)+,D1
		addq.l    #6,A0 
		bpl.s     .lp3 
		add.w     D1,D3 
		bmi		 .done 
		add.w     D1,D1 
		add.w     D1,D1 
		suba.w    D1,A0 
		moveq     #0,D1 
.lp3:	move.w    (_DEV_TAB+2).w,D5		; Maximum addressable height (0 - Ymax)
		subi.w    #$F,D5
		sub.w     D1,D5 
		bge.s     .lp4 
		add.w     D5,D3 
		bmi		 .done 
.lp4:	move.w    D3,(A2) 
		addq.w    #1,(A2)+
		muls	  _bytes_lin.w,D1
		movea.l   v_bas_ad.w,A1 
		adda.l    D1,A1 
		moveq     #0,D4 
		tst.w     D0
		bmi.s     .lp5 
		moveq     #-$13,D4
		add.w     _DEV_TAB.w,D4			; Maximum addressable width
		cmp.w     D0,D4 
		blt.s     .lp5 
		moveq     #-4,D4
		and.w     D0,D4 
.lp5:	movea.l   A1,A4 
		adda.w    D4,A4 
		move.l    A4,(A2)+
		move.w    #$100,(A2)+ 
		movea.w   _bytes_lin.w,A3
		lea		  -20(A3),A3
		move.w    D3,D5 
.lp6:	move.l    (A4)+,(A2)+ 
		move.l    (A4)+,(A2)+ 
		move.l    (A4)+,(A2)+ 
		move.l    (A4)+,(A2)+ 
		move.l    (A4)+,(A2)+ 
		adda.w    A3,A4 
		dbf		  D5,.lp6
		movea.w   _bytes_lin.w,A3
		suba.w    D2,A3 
		subq.w    #1,A3 
		adda.w    D0,A1 
		cmp.w     #$F,D2
		beq.s     .lp7 
		tst.w     D0
		bpl.s     .lp7 
		suba.w    D0,A1 
		NEG.w     D0
		bra.s     .lp8 
		
.lp7:	moveq     #0,D0 
.lp8:	move.w    (A0)+,D4
		move.w    (A0)+,D5
		lsl.w     D0,D4 
		lsl.w     D0,D5 
		move.w    D2,D1 
.lp9:	add.w     D5,D5 
		bcc.s     .lp10 
		move.b    D7,(A1)+
		add.w     D4,D4 
		dbf		 D1,.lp9
		bra.s     .lp12 
		
.lp10:	add.w     D4,D4 
		bcc.s     .lp11 
		move.b    D6,(A1)+
		dbf		 D1,.lp9
		bra.s     .lp12 
.lp11:	addq.l    #1,A1 
		dbf		 D1,.lp9
.lp12:	adda.w    A3,A1 
		dbf		 D3,.lp8
		
.done:	rts 

xbios_dispatcher: ; input: 
		cmp.w     #$15,D0 
		beq.s     xbios_cursconf 

		cmp.w     #$40,D0
		beq.s     xbios_blitmode 

		move.l	xbios_dispatcher_old,-(sp)
		rts
 
xbios_cursconf:
		move.w    (A0)+,D0
		cmp.w     #5,D0 
		bhi.s     .done 
		move.b    .jump(PC,D0.w),D0 
		jsr		  .jump(PC,D0.w)
.done:	RTE
 
.jump:
		dc.b		.l1-.jump,.l2-.jump
		dc.b		.l3-.jump,.l5-.jump
		dc.b		.l6-.jump,.l7-.jump
		
.l1:	bra		hide_cursor
		
.l2:	tst.w     disab_cnt 
		beq.s     .l4 
		move.w    #1,disab_cnt
		bra		 show_cursor 
		
.l3:	BSET		#0,v_stat_0.w
.l4:	rts 

.l5:	BCLR		#0,v_stat_0.w
		rts 
		
.l6		move.b    1(A0),vct_init.w 
		rts 
		
.l7:	moveq     #0,D0 
		move.b    vct_init.w,D0
		rts 
		
xbios_blitmode:
		moveq     #0,D0  ; claim that we have no blitter
		rte 

curtext: ; output a line of text
		movem.l   A2-A3/D1-D3,-(A7) 
		movea.l   4(A0),A3
		move.w    6(A1),D3
		subq.w    #1,D3 
		bmi.s     .l2 
.l1:	move.w    (A3)+,D1
		movea.l   con_state,A0	; load current state
		jsr		 (A0)				; execute it
		dbf		 D3,.l1
.l2:	movem.l   (A7)+,A2-A3/D1-D3 
		rts 

move_cursor:
		bsr.s     hide_cursor 
		move.w    v_cel_mx.w,D2
		tst.w     D0
		bpl.s     .l1 
		moveq     #0,D0 
.l1:	cmp.w     D2,D0 
		BLE.s     .l2 
		move.w    D2,D0 
.l2:	move.w    v_cel_my.w,D2
		tst.w     D1
		bpl.s     .l3 
		moveq     #0,D1 
.l3:	cmp.w     D2,D1 
		BLE.s     .l4 
		move.w    D2,D1 
.l4:	movem.w   D0-D1,v_cur_cx.w 
		movea.l   v_bas_ad.w,A1 
		mulu		v_cel_wr.w,D1
		adda.l    D1,A1 
		lsl.w     #3,D0 
		adda.w    D0,A1 
		move.l    A1,v_cur_ad.w
		bra.s     show_cursor
		 
hide_cursor:
		addq.w    #1,disab_cnt
		cmpi.w    #1,disab_cnt
		bne.s     .l1 
		BCLR		#1,v_stat_0.w
		bne.s     neg_cell 
.l1:	rts 

show_cursor:
		cmpi.w    #1,disab_cnt
		BCS.s     .l2 
		bhi.s     .l1 
		move.b    vct_init.w,v_cur_tim.w
		bsr.s     neg_cell 
		BSET		#1,v_stat_0.w
.l1:	subq.w    #1,disab_cnt
.l2:	rts 

blink:btst		#0,v_stat_0.w
		beq.s     .l1 
		BCHG		#1,v_stat_0.w
		bra.s     neg_cell 
		
.l1:	BSET		#1,v_stat_0.w
		beq.s     neg_cell 
		rts 
		
neg_cell:
		movem.l   A4-A5/A1/D0-D1,-(A7)
		moveq     #$10,D0 
		sub.w     v_cel_ht.w,D0
		add.w     D0,D0 
		move.w    D0,D1 
		add.w     D0,D0 
		add.w     D1,D0 
		movea.l   v_cur_ad.w,A1
		move.w    _bytes_lin.w,D1
		subq.w    #8,D1 
		jmp		 .l1(PC,D0.w)
		
.l1:	not.l     (A1)+ 
		not.l     (A1)+ 
		adda.w    D1,A1 
		not.l     (A1)+ 
		not.l     (A1)+ 
		adda.w    D1,A1 
		not.l     (A1)+ 
		not.l     (A1)+ 
		adda.w    D1,A1 
		not.l     (A1)+ 
		not.l     (A1)+ 
		adda.w    D1,A1 
		not.l     (A1)+ 
		not.l     (A1)+ 
		adda.w    D1,A1 
		not.l     (A1)+ 
		not.l     (A1)+ 
		adda.w    D1,A1 
		not.l     (A1)+ 
		not.l     (A1)+ 
		adda.w    D1,A1 
		not.l     (A1)+ 
		not.l     (A1)+ 
		adda.w    D1,A1 
		not.l     (A1)+ 
		not.l     (A1)+ 
		adda.w    D1,A1 
		not.l     (A1)+ 
		not.l     (A1)+ 
		adda.w    D1,A1 
		not.l     (A1)+ 
		not.l     (A1)+ 
		adda.w    D1,A1 
		not.l     (A1)+ 
		not.l     (A1)+ 
		adda.w    D1,A1 
		not.l     (A1)+ 
		not.l     (A1)+ 
		adda.w    D1,A1 
		not.l     (A1)+ 
		not.l     (A1)+ 
		adda.w    D1,A1 
		not.l     (A1)+ 
		not.l     (A1)+ 
		adda.w    D1,A1 
		not.l     (A1)+ 
		not.l     (A1)+ 
		movem.l   (A7)+,A4-A5/A1/D0-D1
		rts 

do_bell:
		btst		#2,conterm.w 
		beq.s     .l1 
		movea.l   bell_hook.w,A0 
		jmp		 (A0)
.l1:	rts
  
do_tab:		
		andI.w    #-8,D0
		addq.w    #8,D0 
		bra		 move_cursor 

ascii_lf:
		pea		 show_cursor 
		bsr		 hide_cursor 
		sub.w     v_cel_my.w,D1
		beq		 scrup 
		move.w    v_cel_wr.w,D1
		add.l     D1,v_cur_ad.w
		addq.w    #1,v_cur_cy.w
		rts

ascii_cf:
		bsr		 hide_cursor 
		pea		 show_cursor 
		movea.l   v_cur_ad.w,A1

L005A:  lsl.w     #3,D0 
		suba.w    D0,A1 
		move.l    A1,v_cur_ad.w
		clr.w     v_cur_cx.w 
		rts 

; print normal ascii
normal_ascii:
		cmpi.w    #$20,D1 
		bge.s     ascii_out 

		cmpi.w    #27,D1				; escape	
		bne.s     handle_control		; 

		move.l    #esc_ch1,con_state		; state = escape
		rts 

handle_control:
		subq.w    #7,D1				; bell
		subq.w    #6,D1				; 
		bhi.s     .l1					; skip if 14-31
		move.l    #normal_ascii,con_state		; state = normal_ascii
		add.w     D1,D1				
		move.w    .jump(PC,D1.w),D2 
		movem.w   v_cur_cx.w,D0-D1	; cursor X, Y
		jmp		 .jump(PC,D2.w)
.l1:	rts 

		; ascii 0-6 are already filtered out
		dc.w		do_bell-.jump		; ascii 7
		dc.w		escD-.jump			; ascii 8
		dc.w		  do_tab-.jump		; ascii 9
		dc.w		ascii_lf-.jump		; ascii 10
		dc.w		  ascii_lf-.jump		; ascii 11
		dc.w		ascii_lf-.jump		; ascii 12
.jump:	dc.w		ascii_cf-.jump		; ascii 13

con_out:
		movea.l   con_state,A0		; execute current state
		jmp		 (A0)
		
; print raw character
ascii_out:
		movea.l   font_data,A0		
		movea.l   v_cur_ad.w,A1
		move.w    _bytes_lin.w,D2
		subq.w    #8,D2 
		move.b    #2,v_cur_tim.w
		BCLR		#1,v_stat_0.w
		move.l    v_fnt_ad.w,D0
		btst		#4,v_stat_0.w
		bne		 L0067
		cmp.l     font_header,D0	
		bne		 L0068 
		cmpi.l    #$FF,v_col_bg.w
		bne		 L0068 
		lsl.w     #4,D1 
		adda.w    D1,A0 
		move.w    v_cel_ht.w,D1
		subq.w    #1,D1 
		lea		 char_table,A2
.l1:	moveq     #0,D0 
		move.b    (A0)+,D0
		lsl.w     #3,D0 
		move.l    0(A2,D0.w),(A1)+
		move.l    4(A2,D0.w),(A1)+
		adda.w    D2,A1 
		dbf		 D1,.l1
L0063:	move.w    v_cur_cx.w,D0
		cmp.w     v_cel_mx.w,D0
		bge.s     L0064 
		addq.l    #8,v_cur_ad.w
		addq.w    #1,v_cur_cx.w
		moveq     #-1,D0
		rts 
		
L0064:	btst        #3,v_stat_0.w
		beq.s     .l2 
		addq.w    #1,disab_cnt
		EXT.l     D0
		lsl.l     #3,D0 
		sub.l     D0,v_cur_ad.w
		clr.w     v_cur_cx.w 
		move.w    v_cur_cy.w,D1
		pea		 .l1 
		cmp.w     v_cel_my.w,D1
		bge		 scrup 
		addq.l    #4,A7 
		move.w    v_cel_wr.w,D0
		add.l     D0,v_cur_ad.w
		addq.w    #1,v_cur_cy.w
.l1:	subq.w    #1,disab_cnt
.l2:	rts 

L0067:	movem.l   D3-D7,-(A7) 
		lea		 (v_col_bg+1).w,A2
        
	;	MOVEP.w   0(A2),D6
	;	move.b    (A2)+,D6
        move.w    (a2),d6
        move.b    (a2)+,d6
    
		move.w    D6,D4 
		SWAP		D6
		move.w    D4,D6
        
	;	MOVEP.w   1(A2),D5
	;	move.b    1(A2),D5
        move.w    1(a2),d5
        move.b    1(a2),d5
           
		move.w    D5,D3 
		SWAP		D5
		move.w    D3,D5 
		not.l     D5
		not.l     D6
		bra.s     L0069 
		
L0068:	movem.l   D3-D7,-(A7) 
		lea		 (v_col_bg+1).w,A2
        
	;	MOVEP.w   0(A2),D6
	;	move.b    (A2)+,D6
        move.w    0(a2),d6
        move.b    (a2)+,d6
        
		move.w    D6,D4 
		SWAP		D6
		move.w    D4,D6 
        
	;	MOVEP.w   1(A2),D5
	;	move.b    1(A2),D5
        move.w    1(a2),d5
        move.b    1(a2),d5
        
		move.w    D5,D3 
		SWAP		D5
		move.w    D3,D5 
L0069:	movea.l   D0,A0 
		adda.w    D1,A0 
		move.w    v_fnt_wr.w,D0
		move.w    v_cel_ht.w,D1
		subq.w    #1,D1 
		lea		 char_table,A2
.l1:	moveq     #0,D7 
		move.b    (A0),D7 
		lsl.w     #3,D7 
		move.l    0(A2,D7.w),D3 
		move.l    D3,D4 
		not.l     D4
		and.l     D5,D3 
		and.l     D6,D4 
		OR.l		D4,D3 
		move.l    D3,(A1)+
		move.l    4(A2,D7.w),D3 
		move.l    D3,D4 
		not.l     D4
		and.l     D5,D3 
		and.l     D6,D4 
		OR.l		D4,D3 
		move.l    D3,(A1)+
		adda.w    D0,A0 
		adda.w    D2,A1 
		dbf		 D1,.l1
		movem.l   (A7)+,D3-D7 
		bra		 L0063 

; escape sequence		
esc_ch1:
		cmpi.w    #'Y',D1				; ESC+Y: Position cursor
		beq		 L0073 
		move.w    D1,D2 
		movem.w   v_cur_cx.w,D0-D1 
		movea.l   v_cur_ad.w,A1
		movea.w   _bytes_lin.w,A2
		move.l    #normal_ascii,con_state		; state = normal ascii
		subi.w    #'A',D2				;
		cmpi.w    #12,D2				; 
		bhi.s     .l1					;
		
		; ESC-A .. ESC-M
		add.w     D2,D2 
		move.w    .jump1(PC,D2.w),D2 
		jmp		 .jump1(PC,D2.w)
		
		; ESC-b .. ESC-w
.l1:	subi.w    #33,D2 
		cmpi.w    #21,D2 
		bhi.s     .l2 
		add.w     D2,D2 
		move.w    .jump2(PC,D2.w),D2 
		jmp		 .jump2(PC,D2.w)
		
.l2:	rts 

.jump1: ; jumptable for ESC-A .. ESC-L
		dc.w	escA-.jump1, escB-.jump1, escC-.jump1, escD-.jump1
		dc.w	escE-.jump1, escF-.jump1, escG-.jump1, escH-.jump1
		dc.w	escI-.jump1, escJ-.jump1, escK-.jump1, escL-.jump1
		dc.w	escM-.jump1
		
.jump2: ; jumptable for ESC-b .. ESC-w
		dc.w	escb-.jump2, escc-.jump2, escd-.jump2, esce-.jump2
		dc.w	escf-.jump2, escg-.jump2, esch-.jump2, esci-.jump2
		dc.w	escj-.jump2, esck-.jump2, escl-.jump2, escm-.jump2
		dc.w	escn-.jump2, esco-.jump2, escp-.jump2, escq-.jump2
		dc.w	escr-.jump2, escs-.jump2, esct-.jump2, escu-.jump2
		dc.w	escv-.jump2, escw-.jump2

escg:
esch:
esci:
escm:
escn:
escr:
escs:
esct:
escu:
escF:
escG:
		rts
		
escA:
		subq.w    #1,D1 
		bra		 move_cursor
escB:
		addq.w    #1,D1 
		bra		 move_cursor 
escC:
		addq.w    #1,D0 
		bra		 move_cursor 
escD:
		subq.w    #1,D0 
		bra		 move_cursor 
escE:		
		bsr		 hide_cursor 
		bsr		 clear_screen 
		bra.s     cur_home 
escH:  
		bsr		 hide_cursor 
cur_home:
		clr.l     v_cur_cx.w		; v_cur_xy
		movea.l   v_bas_ad.w,A1 
		move.l    A1,v_cur_ad.w	; v_cur_ad
		bra		 show_cursor 
		
escI:
		pea		 show_cursor 
		bsr		 hide_cursor 
		subq.w    #1,D1 
		BLT		 scrdn 
		suba.w    v_cel_wr.w,A1
		move.l    A1,v_cur_ad.w
		move.w    D1,v_cur_cy.w
		rts 
		
escJ:  	bsr.s		escK
		move.w	v_cur_cy.w,d1
		move.w	v_cel_my.w,d2
		sub.w		d1,d2
		beq.s		.l1

		movem.l   A1-A6/D2-D7,-(A7) 
		movea.l   v_bas_ad.w,A1 
		addq.w    #1,D1 
		mulu		v_cel_wr.w,D1
		lsr.l     #2,D1 
		adda.l    D1,A1 
		move.w    D2,D7 
		mulu		v_cel_ht.w,D7
		subq.w    #1,D7 
		bra		 L0083 
.l1:    rts 
		
escK:  	bsr		 hide_cursor 
		move.w    v_cel_mx.w,D2
		sub.w     D0,D2 
		bsr		 erase_line 
		bra		 show_cursor 
		
escL:	pea		 show_cursor 
		bsr		 hide_cursor 
		bsr		 L005A 
		movem.l   A1-A6/D2-D7,-(A7) 
		move.w    v_cel_my.w,D5
		sub.w     D1,D5 
		beq.s     .l1 
		movea.l   v_bas_ad.w,A0 
		movea.l   A0,A1 
		movea.w   _bytes_lin.w,A2
		movea.w   A2,A3 
		move.w    v_cel_ht.w,D0
		mulu		D0,D1 
		move.w    D1,D3 
		add.w     D0,D3 
		move.w    v_cel_mx.w,D4
		lsl.w     #3,D4 
		addq.w    #7,D4 
		mulu		D0,D5 
		subq.w    #1,D5 
		moveq     #3,D7 
		moveq     #0,D0 
		moveq     #0,D2 
		jsr		 scroll 
		movea.l   v_bas_ad.w,A1 
		move.w    v_cur_cy.w,D0
		mulu		v_cel_wr.w,D0
		lsr.l     #2,D0 
		adda.l    D0,A1 
		bra		 L0082 
		
.l1:    movea.l   v_cur_ad.w,A1	
		bra		 L0082 
		
escM:
		pea		 show_cursor 
		bsr		 hide_cursor 
		bsr		 L005A 
		movem.l   A1-A6/D2-D7,-(A7) 
		move.w    v_cel_my.w,D7
		sub.w     D1,D7 
		beq.s     .l1 
		move.w    v_cel_ht.w,D3
		moveq     #0,D0 
		mulu		D3,D1 
		moveq     #0,D2 
		add.w     D1,D3 
		EXG		 D3,D1 
		movem.w   v_cel_mx.w,D4-D5 
		addq.w    #1,D4 
		lsl.w     #3,D4 
		subq.w    #1,D4 
		mulu		v_cel_ht.w,D5
		subq.w    #1,D5 
		sub.w     D1,D5 
		bra		 L007F 
		
.l1:    move.l    A1,D0 
		movea.l   v_bas_ad.w,A1 
		sub.l     A1,D0 
		lsr.l     #2,D0 
		adda.l    D0,A1 
		bra		 L0082 
		
L0073:  move.l    #get_row,con_state		; state = position_cursor, digit #1
		rts 
		
; ESC-Yxy: Set cursor position, receive 1st digit
get_row:
		subi.w    #$20,D1 
		move.w    v_cur_cx.w,D0
		move.l    #get_column,con_state		; state = position_cursor, digit #2
		bra       move_cursor 

get_column:
		subi.w    #$20,D1 
		move.w    D1,D0 
		move.w    v_cur_cy.w,D1
		move.l    #normal_ascii,con_state		; state = normal_ascii
		bra       move_cursor 

escb:	move.l    #.l1,con_state
		rts 
		
.l1:	lea		 v_col_fg.w,A1

L0077:	moveq     #$F,D0
		and.w     D0,D1 
		cmp.w     D0,D1 
		bne.s     .l1
		move.w    #$FF,D1 
.l1:	move.w    D1,(A1) 
		move.l    #normal_ascii,con_state
		rts 
 
escc:	move.l    #.l1,con_state
		rts 
		
.l1:	lea		 v_col_bg.w,A1
		bra.s     L0077 
		
escd:
		bsr.s     esco 
		move.w    v_cur_cy.w,D1
		beq.s     .l1 
		movem.l   A1-A6/D2-D7,-(A7) 
		mulu		v_cel_ht.w,D1
		move.w    D1,D7 
		subq.w    #1,D7 
		movea.l   v_bas_ad.w,A1 
		bra		 L0083 
.l1:	rts
 
esce:	tst.w     disab_cnt 
		beq.s     .l1 
		move.w    #1,disab_cnt
		bra		 show_cursor 
.l1:	rts 

escf:	bra		 hide_cursor 

escj:	BSET		#5,v_stat_0.w
		move.l    v_cur_cx.w,sav_cx.w
		rts
		 
esck:	movem.w   sav_cx.w,D0-D1 
		BCLR		#5,v_stat_0.w
		bne		 move_cursor 
		moveq     #0,D0 
		moveq     #0,D1 
		bra		 move_cursor 
		
escl:	bsr		 hide_cursor 
		bsr		 L005A 
		bsr		 L0081 
		bra		 show_cursor 

esco:   move.w    D0,D2 
		subq.w    #1,D2 
		bmi.s     .l1 
		movea.l   v_bas_ad.w,A1 
		mulu		v_cel_wr.w,D1
		adda.l    D1,A1 
		bra		 erase_line 
.l1:	rts

escp:	BSET		#4,v_stat_0.w
		rts 
	
escq:	BCLR		#4,v_stat_0.w
		rts 
		
escv:	BSET		#3,v_stat_0.w
		rts 
		
escw:	BCLR		#3,v_stat_0.w
		rts 

scrup:	movem.l   A1-A6/D2-D7,-(A7)
		moveq     #0,D0 
		move.w    v_cel_ht.w,D1
		moveq     #0,D2 
		moveq     #0,D3 
		movem.w   v_cel_mx.w,D4-D5 
		addq.w    #1,D4 
		lsl.w     #3,D4 
		subq.w    #1,D4 
		mulu		v_cel_ht.w,D5
		subq.w    #1,D5 
L007F:	moveq     #3,D7 
		movea.l   v_bas_ad.w,A0 
		movea.l   A0,A1 
		movea.w   _bytes_lin.w,A2
		movea.w   A2,A3 
		movea.l   L0090,A6
		jsr		 scroll 
		movea.l   v_bas_ad.w,A1 
		move.w    v_cel_my.w,D0
		mulu		v_cel_wr.w,D0
		adda.l    D0,A1 
		bra.s     L0082 

scrdn:	movem.l   A1-A6/D2-D7,-(A7) 
		moveq     #0,D0 
		moveq     #0,D1 
		moveq     #0,D2 
		move.w    v_cel_ht.w,D3
		movem.w   v_cel_mx.w,D4-D5 
		addq.w    #1,D4 
		addq.w    #1,D5 
		lsl.w     #3,D4 
		subq.w    #1,D4 
		mulu		D3,D5 
		subq.w    #1,D5 
		moveq     #3,D7 
		movea.l   v_bas_ad.w,A0 
		movea.l   A0,A1 
		movea.w   _bytes_lin.w,A2
		movea.w   A2,A3 
		movea.l   L0090,A6
		jsr		 scroll 
		movea.l   v_bas_ad.w,A1 
		bra.s     L0082 
		
L0081:	movem.l   A1-A6/D2-D7,-(A7) 
L0082:	move.w    v_cel_ht.w,D7
		subq.w    #1,D7 
L0083:	lea		 (v_col_bg+1).w,A2

	;	MOVEP.w   0(A2),D6
	;	move.b    (A2),D6 
        move.w    0(a2),d6
        move.b    (a2),d6
        
		move.w    D6,D2 
		SWAP		D6
		move.w    D2,D6 
		move.w    v_cel_mx.w,D4
		move.w    _bytes_lin.w,D5
		move.w    D4,D2 
		addq.w    #1,D2 
		lsl.w     #3,D2 
		sub.w     D2,D5 
.l1:	move.w    D4,D2 
.l2:	move.l    D6,(A1)+
		move.l    D6,(A1)+
		dbf		 D2,.l2
		adda.w    D5,A1 
		dbf		 D7,.l1
		movem.l   (A7)+,A1-A6/D2-D7 
		rts 

clear_screen:
		movem.l   A1-A6/D2-D7,-(A7) 
		move.w    v_cel_my.w,D7     ; V_CEL_MY
		addq.w    #1,D7 
		mulu      v_cel_ht.w,D7     ; V_CEL_HT
		subq.w    #1,D7 
		movea.l   v_bas_ad.w,A1 
		bra.s     L0083 

erase_line:
		move.l    D3,-(A7)
		lea		 (v_col_bg+1).w,A0
        
	;	MOVEP.w   0(A0),D3
	;	move.b    (A0),D3 
        move.w    0(a0),d3
        move.b    (a0),d3
           
		move.w    D3,D0 
		SWAP		D3
		move.w    D0,D3 
		move.w    v_cel_ht.w,D1
		subq.w    #1,D1 
		move.w    D2,D0 
		addq.w    #1,D0 
		lsl.w     #3,D0 
		movea.w   _bytes_lin.w,A0
		suba.w    D0,A0 
.l1:	move.w    D2,D0 
.l2:	move.l    D3,(A1)+
		move.l    D3,(A1)+
		dbf		 D0,.l2
		adda.w    A0,A1 
		dbf		 D1,.l1
		move.l    (A7)+,D3
		rts 

; init character render lookup
init_char:
		movem.l   A0-A1/D0-D2,-(A7) 
		lea		 char_table,A0
		moveq     #0,D0 
.l1:	move.w    D0,D1 
		moveq     #7,D2 
.l2:	clr.b     (A0)
		add.b     D1,D1 
		bcc.s     .l3 
		not.b     (A0)
.l3:	addq.l    #1,A0 
		dbf		 D2,.l2
		addq.w    #1,D0 
		cmp.w     #$100,D0
		blt.s     .l1 
		movem.l   (A7)+,A0-A1/D0-D2 
		rts 

vdi_escape:
		movea.l   (A0),A1 
		move.w    10(A1),D0 
		cmp.w     #$F,D0
		bhi.s     .l1 
		
		movem.l   A2-A5/D1-D7,-(A7) 
		movem.l   4(A0),A2-A5 
		add.w     D0,D0 
		move.w    esc_lookup(PC,D0.w),D2 
		movea.l   A2,A5 
		movea.l   A1,A0 
		movem.w   v_cur_cx.w,D0-D1 
		movea.l   v_cur_ad.w,A1
		movea.w   _bytes_lin.w,A2
		jsr		 esc_lookup(PC,D2.w)
		movem.l   (A7)+,A2-A5/D1-D7 
		rts 
		
.l1:	movea.l   vdi_escape_old,A1
		jmp		 (A1)
	
esc_lookup:
		dc.w		invalid-esc_lookup,chcells-esc_lookup,exit_alpha-esc_lookup,enter_alpha-esc_lookup
		dc.w		escA-esc_lookup,escB-esc_lookup,escC-esc_lookup,escD-esc_lookup
		dc.w		escH-esc_lookup,escJ-esc_lookup,escK-esc_lookup,set_curadr-esc_lookup 
		dc.w		curtext-esc_lookup,escp-esc_lookup,escq-esc_lookup,get_curadr-esc_lookup

invalid: 
		rts
			
chcells:
		move.l    v_cel_mx.w,D3
		ADDI.l    #$10001,D3
		SWAP		D3
		move.l    D3,(A4) 
		move.w    #2,8(A0)
		rts 
		
exit_alpha: 
		addq.w    #1,disab_cnt
		BCLR		#1,v_stat_0.w
		bra		 clear_screen 
		
enter_alpha:
		clr.l     v_cur_cx.w 
		move.l    v_bas_ad.w,v_cur_ad.w
		move.l    #normal_ascii,con_state
		bsr		 clear_screen 
		BCLR		#1,v_stat_0.w
		move.w    #1,disab_cnt
		bra		 show_cursor 
		
set_curadr:
		move.w    (A5)+,D1
		move.w    (A5)+,D0
		subq.w    #1,D0 
		subq.w    #1,D1 
		bra		 move_cursor 
		
get_curadr:
		addq.w    #1,D0 
		addq.w    #1,D1 
		move.w    D1,(A4)+
		move.w    D0,(A4)+
		move.w    #2,8(A0)
		rts 

; wrapper used when scrolling the esc screen
scroll:
		movea.l   L0091,A6
		move.w    D7,494(A6)
		move.l    A0,450(A6)
		move.l    A1,470(A6)
		move.w    A2,454(A6)
		move.w    A3,474(A6)
		move.w    #$F,456(A6) 
		move.w    #$F,476(A6) 
		move.w    D4,D6 
		move.w    D5,D7 
		
		movea.l	524(A6),a0				; blit
		jmp		 (A0)

init_console:
		movem.l   A0-A2/D0-D4,-(A7)	; called during init
		movea.l   X0043,A0
		movea.l   36(A0),A0 
		move.l    6(A0),font_header 
		move.l    10(A0),font_data
		; move.l    14(A0),font_unused

		move.l    #normal_ascii,con_state
		
		movem.l   (A7)+,A0-A2/D0-D4 
		rts 

        EVEN
        
		DATA 

name:
		dc.b		'Vampire V4 256 colours (Packed)',0 
        
        BSS

regs_save:      ds.l      16

X0040:			DS.w		1 
X0041:			DS.w		1
X0042:			DS.w		1 
X0043:			DS.l		1 
X0044:			DS.l		1 
xbios_dispatcher_old:
                DS.l		1 
X0046:			DS.b		16
X0047:			DS.l		1 
X0048:			DS.l		1 

vdi_escape_old:	DS.l		1

L012A:			DS.b		20
L012B:			DS.b		16
L012C:			DS.b		328 
L0090:			DS.l		1 
L0091:			DS.l		1

disab_cnt:		DS.w		1	; cursor semaphore
con_state:		DS.l		1	; console state pointer (function pointer)
L0131:			DS.l		1 
char_table:		DS.b		2048

font_header:			DS.l		1 
font_data:			DS.l		1 
; font_unused:			DS.l		1 

		BSS
		
