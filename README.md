# NVDI drivers for the Vampire V4SA #

These are some rudimenary NVDI drivers for the V4SA + EmuTOS.

### How do I get set up? ###

1. Install NVDI v5.03 (select Atari graphics).
2. Copy V4SCREEN.SYS and V4_BPSxx.SYS to your GEMSYS folder.
3. Replace ASSIGN.SYS with the one from this project.
4. Reboot.

Note that in order to be able to support "new" screen resolutions with pixel depths/formats >= 8bpp, you'll need a fairly recent EmuTOS image. In fact, the one you need isn't even distributed yet.

### Building this stuff ###

You'll need:

- GCC (cross) compiler
- VASM
- VLINK

Steps to build:

1. Build V4SCREEN.SYS (make -f makefile.v4)
2. Build 'reloctab' (make)
3. Build V4_BPSxx.SYS (make). This step requires that you've built reloctab (above).

Caveat:
Make sure ASSIGN.SYS ends up having Atari/DOS line endings. NVDI will halt abruptly during boot on UNIX endings. GIT screws this up.

### fcookie.ttp ###

Older versions of XaAES won't behave properly unless it finds a \_VDO cookie with the value 30000h. This utility can be used to force a new cookie value to an existing cookie.

### Who do I talk to? ###

If you have ideas or suggestions, don't hesitate to contact me at pep.fishmoose@gmail.com, or at the Vampire discorde (Shoggoth). Pull requests are welcome.